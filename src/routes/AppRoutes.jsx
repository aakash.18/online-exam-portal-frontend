import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AppLayout from "../component/admin/AppLayout";
import DeleteModall from "../component/admin/DeleteModall";
import FilterModal from "../component/admin/FilterModal";
import SearchBar from "../component/admin/SearchBar";
import Dashboard from "../component/admin/dashboard/Dashboard";
import CreateExam from "../component/admin/exam/CreateExam";
import DynamicInput from "../component/admin/exam/DynamicInput";
import Exam from "../component/admin/exam/Exam";
import ExamQuestion from "../component/admin/exam/ExamQuestion";
import ExamTable from "../component/admin/exam/ExamTable";
import ShowStudentExam from "../component/admin/exam/ShowStudentExam";
import StudentProgrammingResponse from "../component/admin/exam/StudentProgrammingResponse";
import UpdateExam from "../component/admin/exam/UpdateExam";
import CreateQuestion from "../component/admin/question/CreateQuestion";
import ProgrammingQuestion from "../component/admin/question/ProgrammingQuestion";
import ProgrammingQuestionDetail from "../component/admin/question/ProgrammingQuestionDetail";
import Question from "../component/admin/question/Question";
import QuestionDetail from "../component/admin/question/QuestionDetail";
import QuestionTable from "../component/admin/question/QuestionTable";
import SelectCreateQuestion from "../component/admin/question/SelectCreateQuestion";
import UpdateQuestion from "../component/admin/question/UpdateQuestion";
import AdmitCard from "../component/admin/result/AdmitCard";
import AdmitCardDetail from "../component/admin/result/AdmitCardDetail";
import FinalResult from "../component/admin/result/FinalResult";
import Result from "../component/admin/result/Result";
import ResultDetail from "../component/admin/result/ResultDetail";
import ResultDetailTable from "../component/admin/result/ResultDetailTable";
import CreateStudent from "../component/admin/student/CreateStudent";
import Student from "../component/admin/student/Student";
import StudentTable from "../component/admin/student/StudentTable";
import UpdateStudent from "../component/admin/student/UpdateStudent";
import ViewStudent from "../component/admin/student/ViewStudent";
import Login from "../component/auth/Login";
import SearchStudentExam from "../component/student/Exam/SearchStudentExam";
import StudentExam from "../component/student/Exam/StudentExam";
import StudentExamTable from "../component/student/Exam/StudentExamTable";
import StudentExamDashboard from "../component/student/ExamDashboard/StudentExamDashboard";
import QuestionComponent from "../component/student/ExamQuestions/QuestionComponent";
import StudentExamQuestionLayout from "../component/student/ExamQuestions/StudentExamQuestionsLayout";
import StudentRigntSideBarLayout from "../component/student/ExamQuestions/StudentRigntSideBarLayout";
import Home from "../component/student/Home";
import StudentProfile from "../component/student/Profile/StudentProfile";
import StudentResult from "../component/student/Result/StudentResult";
import StudentResultTable from "../component/student/Result/StudentResultTable";
import StudentDashboard from "../component/student/dashboard/StudentDashboard";
import ProtectedRoute from "./ProtectedRoute";

const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route
          path="/student"
          element={
            <ProtectedRoute>
              <Home />
            </ProtectedRoute>
          }
        >
          <Route index element={<StudentDashboard />} />
          <Route path="exam" element={<StudentExam />} />
          <Route path="result" element={<StudentResult />} />
          <Route path="profile" element={<StudentProfile />} />
          <Route path="searcexam" element={<SearchStudentExam />} />
          <Route path="examtable" element={<StudentExamTable />} />
          <Route path="result-detail/:examId" element={<ExamQuestion isStudentResultDetail={true}/>} />
        </Route>
        <Route
          path="/student/exam-dashboard/:examId"
          element={<StudentExamDashboard />}
        />

        <Route
          path="/student/exam/:examId"
          element={<StudentExamQuestionLayout />}
        />
        <Route path="/question" element={<QuestionComponent />} />
        <Route path="/studentresulttable" element={<StudentResultTable />} />

        <Route path="/rightsidebar" element={<StudentRigntSideBarLayout />} />
        <Route
          path="/admin"
          element={
            <ProtectedRoute>
              <AppLayout />
            </ProtectedRoute>
          }
        >
          <Route index element={<Dashboard />} />
          <Route path="question" element={<Question />} />
          <Route path="exam" element={<Exam />} />
          <Route path="result" element={<Result />} />
          <Route path="questiontable" element={<QuestionTable />} />
          <Route path="examtable" element={<ExamTable />} />
          <Route path="updatequestion" element={<UpdateQuestion />} />
          <Route path="updatestudent" element={<UpdateStudent />} />
          <Route path="createquestion" element={<CreateQuestion />} />
          <Route path="createstudent" element={<CreateStudent />} />
          <Route path="studenttable" element={<StudentTable />} />
          <Route path="createexam" element={<CreateExam />} />
          <Route path="student" element={<Student />} />
          <Route path="searchbar" element={<SearchBar />} />
          <Route path="result" element={<Result />} />
          <Route path="result-detail/:examId" element={<ResultDetailTable />} />
          <Route path="all-result-detail/:examId" element={<ResultDetail />} />
          <Route path="admitcard" element={<AdmitCard />} />
          <Route path="admitcarddetail" element={<AdmitCardDetail />} />
          <Route path="questiondetail" element={<QuestionDetail />} />
          <Route path="deletemodal" element={<DeleteModall />} />
          <Route path="filtermodal" element={<FilterModal />} />
          <Route path="dynamicinput" element={<DynamicInput />} />
          <Route path="updateexam" element={<UpdateExam />} />
          <Route
            path="select-create-question"
            element={<SelectCreateQuestion />}
          />
          <Route path="exam-questions/:examId" element={<ExamQuestion isStudentResultDetail={false}/>} />
          <Route path="viewstudent" element={<ViewStudent />} />
          <Route path="student-exam/:examId" element={<ShowStudentExam />} />
          <Route
            path="student-exam/:examId/:userId"
            element={<StudentProgrammingResponse />}
          />
          <Route path="programmingquestion" element={<ProgrammingQuestion />} />
          <Route path="final-result" element={<FinalResult />} />
          <Route
            path="programming-question-detail"
            element={<ProgrammingQuestionDetail />}
          />
          <Route path="student/home" element={<Home />} />
          <Route path="student/dashboard" element={<StudentDashboard />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
