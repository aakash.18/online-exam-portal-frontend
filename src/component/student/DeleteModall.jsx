import React from 'react'

const DeleteModall = (props) => {
  return (
    <div>
      Are You Sure You want to delete this {props.value} ?
    </div>
  )
}

export default DeleteModall
