import { Card, Col, Divider, Row } from "antd";
import Meta from "antd/es/card/Meta";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import StudentResultTable from "../Result/StudentResultTable";

const StudentDashboard = () => {
  const { setIsLoading } = useOutletContext();
  const [studentDashboardDetail, setStudentDashboardDetail] = useState();
  useEffect(() => {
    setIsLoading(true);
    axios
      .get(
        `${"http://localhost:8899"}/user/student-dashboard/${localStorage.getItem(
          "userId"
        )}`
      )
      .then((response) => {
        console.log(response.data.data);
        setStudentDashboardDetail(response.data.data);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return (
    <>
      {studentDashboardDetail && (
        <Row>
          <div>
            <Row>
              <Col span={5}>
                <h4>Welcome Student!</h4>
              </Col>
            </Row>
            <Row style={{ marginTop: "20px" }}>
              <Col span={8}>
                <Card
                  hoverable
                  style={{ width: 280, height: 100, background: "#6943ce" }}
                >
                  <Meta
                    title={studentDashboardDetail.totalGivenExams.toString()}
                    description="Total Given Exams"
                  />
                </Card>
              </Col>
              <Col span={8}>
                <Card
                  hoverable
                  style={{ width: 280, height: 100, background: "#6943ce" }}
                >
                  <Meta
                    title={studentDashboardDetail.totalPassedExams.toString()}
                    description="Total Passed Exams"
                  />
                </Card>
              </Col>

              <Col span={8}>
                <Card
                  hoverable
                  style={{ width: 280, height: 100, background: "#6943ce" }}
                >
                  <Meta
                    title={studentDashboardDetail.totalFailedExams.toString()}
                    description="Total Failed Exams"
                  />
                </Card>
              </Col>
              <Divider />
            </Row>
            <Row>
              <Col span={24} style={{textAlign: "start"}}>
                <h4>Recent Exam Results</h4>
              </Col>
            </Row>
            <StudentResultTable
              resultList={studentDashboardDetail.responseResultDetailDtoList}
              setResultList={null}
            />
          </div>
        </Row>
      )}
    </>
  );
};

export default StudentDashboard;
