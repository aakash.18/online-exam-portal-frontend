import { Button, Col, Divider, Form, Row, Space, message } from "antd";
import TextArea from "antd/es/input/TextArea";
import axios from "axios";
import React from "react";
import { GrFormPrevious } from "react-icons/gr";
import { IoIosArrowForward } from "react-icons/io";
import { useNavigate } from "react-router-dom";

const ProgrammingQuestionComponent = ({
  examId,
  currentQuestion,
  setCurrentQuestion,
  examQuestionList,
  setExamQuestionList,
  ans,
  setAns,
}) => {
  const AnswerStatus = {
    ANSWERED: 0,
    NOT_ANSWERED: 1,
    NOT_VISITED: 2,
  };

  const navigate = useNavigate();

  const onSubmitExam = () => {
    axios
      .post(`${"http://localhost:8899"}/result/create-result`, {
        examId: examId,
        userId: localStorage.getItem("userId"),
      })
      .then((response) => {
        console.log(response.data.data);
      });
    message.success("Your programm is savd");
    // navigate("/student/result-detail");
  };

  const onSaveAndNext = () => {
    if (!ans) return;
    axios
      .post(`${"http://localhost:8899"}/selected-answer/`, {
        selectedAnswer: ans,
        examId: examId,
        userId: localStorage.getItem("userId"),
        questionId: examQuestionList[currentQuestion].questionId,
      })
      .then((res) => {
        setExamQuestionList((prev) =>
          prev.map((e, i) => {
            if (i === currentQuestion) {
              return {
                ...e,
                selectedAns: ans,
                answerStatus: AnswerStatus.ANSWERED,
              };
            } else return e;
          })
        );
        if (currentQuestion === examQuestionList.length - 1) return;
        setCurrentQuestion((prev) => prev + 1);
        setAns(examQuestionList[currentQuestion + 1].selectedAns);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onSkip = () => {
    setCurrentQuestion((prev) => prev + 1);
    setAns(examQuestionList[currentQuestion + 1].selectedAns);
    setExamQuestionList((prev) =>
      prev.map((e, i) => {
        if (i === currentQuestion) {
          return {
            ...e,
            selectedAns: null,
            answerStatus: AnswerStatus.NOT_ANSWERED,
          };
        } else return e;
      })
    );
  };

  return (
    <div>
      <div
        style={{
          paddingLeft: "40px",
          paddingTop: "70px",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <h5>Question {currentQuestion + 1}</h5>
      </div>
      <Divider
        style={{
          borderWidth: 1,
          borderColor: "black",
          marginLeft: "40px",
        }}
      />
      <div
        style={{
          marginLeft: "40px",
          width: "500px",
          paddingBottom: "10px",
          fontWeight: "bold",
        }}
      >
        Problem statement : {examQuestionList[currentQuestion].question}
      </div>
      <Form>
        <Row style={{ marginLeft: "40px", width: "800px" }}>
          <Col span={24}>
            <Form.Item name="ans">
              <TextArea
                onPaste={(e)=>{
                  e.preventDefault()
                  return false;
                }} 
                onCopy={(e)=>{
                  e.preventDefault()
                  return false;
                }}
                value={ans}
                onChange={(e) => {
                  setAns(e.target.value);
                }}
                rows={8}
                placeholder="Write your Code here"
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <div style={{ height: "20px" }} />
      <Divider
        style={{
          borderWidth: 1,
          borderColor: "black",
          marginLeft: "40px",
        }}
      />
      <Space
        direction="horizontal"
        align="start"
        style={{
          marginLeft: "40px",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Button
          type="primary"
          style={{ background: "black", color: "white" }}
          disabled={currentQuestion === 0}
          onClick={() => {
            setAns(examQuestionList[currentQuestion - 1].selectedAns);
            setCurrentQuestion((prevQuestionCount) => prevQuestionCount - 1);
          }}
        >
          <GrFormPrevious /> Previous
        </Button>

        <Space direction="horizontal" align="end">
          {currentQuestion !== examQuestionList.length - 1 && (
            <Button
              type="primary"
              style={{ background: "orange", width: "120px" }}
              onClick={onSkip}
            >
              Skip
            </Button>
          )}
          <Button
            type="primary"
            style={{ background: "green", color: "white" }}
            onClick={onSaveAndNext}
          >
            {currentQuestion === examQuestionList.length - 1
              ? "Save"
              : "Save & Next"}
            <IoIosArrowForward />
          </Button>
        </Space>
      </Space>
      <div
        style={{
          marginTop: "10px",
          width: "600",
          display: "flex",
          justifyContent: "end",
        }}
      ></div>
    </div>
  );
};

export default ProgrammingQuestionComponent;
