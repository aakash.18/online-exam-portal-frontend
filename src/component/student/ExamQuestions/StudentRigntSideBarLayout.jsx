import { Avatar, Button, Col, Divider, Row, Space, Statistic } from "antd";
import React from "react";
const { Countdown } = Statistic;

const StudentRigntSideBatLayout = ({
  questionList,
  setQuestionList,
  currentQuestion,
  setCurrentQuestion,
  ans,
  setAns,
  duration,
}) => {
  const onFinish = () => {
    console.log("finished!");
  };

  return (
    <>
      <div
        style={{
          marginRight: "10",
          marginTop: "20",
          width: "300px",
        }}
      >
        <Space direction="horizontal" align="start">
          <Avatar size={30} style={{ background: "green" }}></Avatar>
          <span>Answered</span>
          <Avatar size={30} style={{ background: "red" }}></Avatar>
          <span>Not Answered</span>
        </Space>

        <div>
          <Avatar
            size={30}
            style={{
              background: "#8B8B8B",
              marginRight: "5px",
              marginTop: "5px",
            }}
          ></Avatar>
          <span>Not Visited</span>
        </div>
        {/* <Space direction="horizontal" align="start">
        <Avatar size={30} style={{background:"green"}}></Avatar>
        <span>Skip</span>
        </Space> */}
        <Divider></Divider>

        <Row>
          {questionList.map((examQuestion, index) => (
            <Col span={4}>
              <Button
                type="primary"
                shape="circle"
                style={{
                  marginBottom: "5px",
                  background:
                    examQuestion.answerStatus === 0
                      ? "green"
                      : examQuestion.answerStatus === 1
                      ? "red"
                      : "#8B8B8B",
                }}
                onClick={() => {
                  setCurrentQuestion(index);
                  setAns(examQuestion.selectedAns);
                }}
              >
                {index + 1}
              </Button>
            </Col>
          ))}
        </Row>
      </div>
    </>
  );
};

export default StudentRigntSideBatLayout;
