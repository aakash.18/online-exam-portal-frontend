import { Button, Col, Modal, Row, Space, Statistic, message } from "antd";
import { Header } from "antd/es/layout/layout";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import ProgrammingQuestionComponent from "./ProgrammingQuestionComponent";
import QuestionComponent from "./QuestionComponent";
import "./StudentExamQuestionsLayout.css";
import StudentRigntSideBarLayout from "./StudentRigntSideBarLayout";
const { Countdown } = Statistic;

const ExamQuestionsLayout = ({ examId }) => {
  const navigate = useNavigate();
  const params = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [examQuestionList, setExamQuestionList] = useState();
  const [categoryPassingMarksList, setCategoryPassinMarksList] = useState();
  const [currentMcqQuestion, setCurrentMcqQuestion] = useState(0);
  const [currentProgrammingQuestion, setCurrentProgrammingQuestion] =
    useState(0);
  const [mcqAns, setMcqAns] = useState();
  const [programmingAns, setProgrammingAns] = useState();
  const [isMcq, setIsMcq] = useState(true);
  const [mcqQuestionList, setMcqQuestionList] = useState();
  const [programmingQuestionList, setProgrammingQuestionList] = useState();
  const [duration, setDuration] = useState();
  const [viewModal, setViewModal] = useState();

  useEffect(() => {
    axios
      .get(`${"http://localhost:8899"}/exam/${params.examId}`)
      .then((response) => {
        console.log(response.data.data);
        setExamQuestionList(response.data.data.questionDtoList);
        setDuration(response.data.data.duration + Date.now());
        setMcqQuestionList(
          response.data.data.questionDtoList.filter(
            (question) =>
              question.typeOfCategory === "Logical" ||
              question.typeOfCategory === "technical"
          )
        );
        setProgrammingQuestionList(
          response.data.data.questionDtoList.filter(
            (question) => question.typeOfCategory === "Programming"
          )
        );
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  const onSubmitExam = () => {
    console.log("On submit exam");
    axios
      .post(`${"http://localhost:8899"}/result/create-result`, {
        examId: params.examId,
        userId: localStorage.getItem("userId"),
      })
      .then((response) => {
        message.success(
          "your result will be seen in result section once the Admin authenticate it"
        );
        navigate("/student");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <Header
        style={{
          padding: 0,
          background: "primary",
          position: "sticky",
          paddingTop: 10,
          paddingRight: 10,
          paddingLeft: 20,
          top: 0,
          zIndex: 1,
          height: 80,
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start",
        }}
      >
        <Row>
          <Col>
            <h4 style={{ color: "white" }}>ONLINE </h4>
          </Col>
          <div style={{ width: "10px" }} />
          <Col>
            <h4 style={{ color: "white" }}>EXAM </h4>
          </Col>
          <div style={{ width: "20px" }} />
          <Col>
            <h4 style={{ color: "white" }}>PORTAL </h4>
          </Col>
          <div style={{ width: "40px" }} />
          <Col>
            <h4 style={{ color: "white" }}>
              <Button
                type="primary"
                style={{ height: "40px" }}
                onClick={() => setIsMcq(true)}
              >
                MCQ
              </Button>
            </h4>
          </Col>
          <div style={{ width: "20px" }} />
          <Col>
            <h4 style={{ color: "white" }}>
              <Button
                type="primary"
                style={{ height: "40px" }}
                onClick={() => setIsMcq(false)}
              >
                Programming
              </Button>
            </h4>
          </Col>
          <div style={{ width: "20px" }} />
          <Col>
            <h4 style={{ color: "white" }}>
              <Button
                type="primary"
                style={{ height: "40px", background: "green" }}
                onClick={onSubmitExam}
              >
                Submit Exam
              </Button>
            </h4>
          </Col>
        </Row>
      </Header>
      <div className="screen-padding">
        {isMcq ? (
          <Row>
            <Col span={16}>
              {mcqQuestionList && (
                <QuestionComponent
                  examId={params.examId}
                  mcqQuestionList={mcqQuestionList}
                  setMcqQuestionList={setMcqQuestionList}
                  currentQuestion={currentMcqQuestion}
                  setCurrentQuestion={setCurrentMcqQuestion}
                  ans={mcqAns}
                  setAns={setMcqAns}
                />
              )}
            </Col>
            <Col
              span={8}
              style={{ display: "flex", justifyContent: "flex-end" }}
            >
              <Space direction="vertical">
                <Countdown
                  style={{
                    marginTop: "20px",
                    marginRight: "10px",
                    marginBottom: "10px",
                    height: "40px",
                    paddingLeft: "10px",
                    paddingRight: "10px",
                  }}
                  value={duration}
                  onFinish={() => setViewModal(true)}
                />
                {mcqQuestionList && (
                  <StudentRigntSideBarLayout
                    questionList={mcqQuestionList}
                    setQuestionList={setMcqQuestionList}
                    currentQuestion={currentMcqQuestion}
                    setCurrentQuestion={setCurrentMcqQuestion}
                    ans={mcqAns}
                    setAns={setMcqAns}
                    duration={duration}
                  />
                )}
              </Space>
            </Col>
          </Row>
        ) : (
          <Row>
            <Col span={16}>
              {programmingQuestionList && (
                <ProgrammingQuestionComponent
                  examId={params.examId}
                  examQuestionList={programmingQuestionList}
                  setExamQuestionList={setProgrammingQuestionList}
                  currentQuestion={currentProgrammingQuestion}
                  setCurrentQuestion={setCurrentProgrammingQuestion}
                  ans={programmingAns}
                  setAns={setProgrammingAns}
                />
              )}
            </Col>
            <Col
              span={8}
              style={{ display: "flex", justifyContent: "flex-end" }}
            >
              <Space direction="vertical">
                <Countdown
                  style={{
                    marginTop: "20px",
                    marginRight: "10px",
                    marginBottom: "10px",
                    height: "40px",
                    paddingLeft: "10px",
                    paddingRight: "10px",
                  }}
                  value={duration}
                  onFinish={() => setViewModal(true)}
                />
                {mcqQuestionList && (
                  <StudentRigntSideBarLayout
                    questionList={programmingQuestionList}
                    setQuestionList={setProgrammingQuestionList}
                    currentQuestion={currentProgrammingQuestion}
                    setCurrentQuestion={setCurrentProgrammingQuestion}
                    ans={programmingAns}
                    setAns={setProgrammingAns}
                    duration={duration}
                  />
                )}
              </Space>
            </Col>
          </Row>
        )}
      </div>
      <Modal
        title="Oops Exam Finished"
        style={{
          width: 700,
        }}
        centered
        open={viewModal}
        onOk={onSubmitExam}
        closable={false}
        cancelButtonProps={{ style: { display: "none" } }}
      >
        Your Exam has been finished
      </Modal>
    </div>
  );
};

export default ExamQuestionsLayout;
