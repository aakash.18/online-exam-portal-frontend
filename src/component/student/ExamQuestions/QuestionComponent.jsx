import { Button, Divider, Form, Modal, Radio, Space, message } from "antd";
import axios from "axios";
import React, { useState } from "react";
import { GrFormPrevious } from "react-icons/gr";
import { IoIosArrowForward } from "react-icons/io";
import { useNavigate } from "react-router-dom";

const AnswerStatus = {
  ANSWERED: 0,
  NOT_ANSWERED: 1,
  NOT_VISITED: 2,
};

const QuestionComponent = ({
  examId,
  mcqQuestionList,
  setMcqQuestionList,
  currentQuestion,
  setCurrentQuestion,
  ans,
  setAns,
}) => {
  const navigate = useNavigate();

  const [deleteModal, setdeleteModal] = useState(false);

  const [formRef] = Form.useForm();

  const onSkip = () => {
    setCurrentQuestion((prev) => prev + 1);
    setAns(mcqQuestionList[currentQuestion + 1].selectedAns);
    setMcqQuestionList((prev) =>
      prev.map((e, i) => {
        if (i === currentQuestion) {
          return {
            ...e,
            selectedAns: null,
            answerStatus: AnswerStatus.NOT_ANSWERED,
          };
        } else return e;
      })
    );
  };

  const onSaveAndNext = () => {
    if (!ans) return;
    axios
      .post(`${"http://localhost:8899"}/selected-answer/`, {
        selectedAnswer: ans,
        examId: examId,
        userId: localStorage.getItem("userId"),
        questionId: mcqQuestionList[currentQuestion].questionId,
      })
      .then((res) => {
        setMcqQuestionList((prev) =>
          prev.map((e, i) => {
            if (i === currentQuestion) {
              return {
                ...e,
                selectedAns: ans,
                answerStatus: AnswerStatus.ANSWERED,
              };
            } else return e;
          })
        );
        if (currentQuestion === mcqQuestionList.length - 1) return;
        setCurrentQuestion((prev) => prev + 1);
        setAns(mcqQuestionList[currentQuestion + 1].selectedAns);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <>
        <div
          style={{
            paddingLeft: "40px",
            paddingTop: "70px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <h5>Question {currentQuestion + 1}</h5>
        </div>
        <Divider
          style={{
            borderWidth: 1,
            borderColor: "black",
            marginLeft: "40px",
          }}
        />
        <div style={{ marginLeft: "40px", width: "500px" }}>
          {mcqQuestionList[currentQuestion].question}
        </div>
        <div style={{ height: "20px" }} />
        <Space
          direction="horizontal"
          align="start"
          style={{
            marginLeft: "40px",
            width: "400px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Form ref={formRef}>
            <Radio.Group value={ans} onChange={(e) => setAns(e.target.value)}>
              <Radio key={1} value={"a"}>
                ( A ) {mcqQuestionList[currentQuestion].opt1}{" "}
              </Radio>

              <Radio key={2} value={"b"}>
                ( B ) {mcqQuestionList[currentQuestion].opt2}{" "}
              </Radio>
              <br />
              <Radio key={3} value={"c"}>
                ( C ) {mcqQuestionList[currentQuestion].opt3}{" "}
              </Radio>
              <Radio key={4} value={"d"}>
                ( D ) {mcqQuestionList[currentQuestion].opt4}{" "}
              </Radio>
            </Radio.Group>
          </Form>
        </Space>
        <div style={{ height: "100px" }} />
        <Divider
          style={{
            borderWidth: 1,
            borderColor: "black",
            marginLeft: "40px",
          }}
        />
        <Space
          direction="horizontal"
          align="start"
          style={{
            marginLeft: "40px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Button
            type="primary"
            style={{ background: "black", color: "white" }}
            disabled={currentQuestion === 0}
            onClick={() => {
              setAns(mcqQuestionList[currentQuestion - 1].selectedAns);
              setCurrentQuestion((prevQuestionCount) => prevQuestionCount - 1);
            }}
          >
            <GrFormPrevious /> Previous
          </Button>
          <Button
            type="primary"
            style={{ background: "green", color: "white" }}
            onClick={onSaveAndNext}
          >
            {currentQuestion === mcqQuestionList.length - 1
              ? "Save"
              : "Save & Next"}
            <IoIosArrowForward />
          </Button>
        </Space>
        {currentQuestion !== mcqQuestionList.length - 1 && (
          <div
            style={{
              marginTop: "10px",
              width: "600",
              display: "flex",
              justifyContent: "end",
            }}
          >
            <Button
              type="primary"
              style={{ background: "orange", width: "120px" }}
              onClick={onSkip}
            >
              Skip
            </Button>
          </div>
        )}
      </>

      <Modal
        title="Submit"
        style={{
          top: 20,
          width: 700,
        }}
        centered
        open={deleteModal}
        onOk={() => setdeleteModal(navigate("/result"))}
        onCancel={() => setdeleteModal(false)}
        okText="Yes"
        cancelText="No"
      >
        Are you sure , you want to submit this exam ?
      </Modal>
    </div>
  );
};

export default QuestionComponent;
