import React from 'react'

const ConfirmationModal = (props) => {
  return (
    <div>
      Are You Sure You want to submit this {props.value} ?
    </div>
  )
}

export default DeleteModall