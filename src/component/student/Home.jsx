import React, { useState } from "react";
import { CiBank } from "react-icons/ci";
import { LuLogOut } from "react-icons/lu";
import { PiExamLight, PiStudent } from "react-icons/pi";
import { RxDashboard } from "react-icons/rx";

import { Avatar, Image, Layout, Menu, Spin, message, theme } from "antd";
import { Outlet, useNavigate } from "react-router-dom";
import "../admin/AppLayout.css";

const { Header, Content, Footer, Sider } = Layout;

const AppLayout = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  return (
    <Layout hasSider style={{ minWidth: 900 }}>
      <Sider
        className="sidebar"
        width={200}
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
          top: 0,
          bottom: 0,
        }}
      >
        <Image
          src="/logo.jpg"
          preview={false}
          style={{
            padding: 20,
            margin: 0,
          }}
        />

        <div className="demo-logo-vertical" />

        <Menu
          className="menu"
          mode="inline"
          defaultSelectedKeys={["4"]}
          theme="dark"
          items={[
            {
              key: "dashboard",
              icon: <RxDashboard />,
              label: "Dashboard",
              onClick: () => navigate("/student"),
            },
            {
              key: "exam",
              icon: <CiBank />,
              label: "Exam",
              onClick: () => navigate("/student/exam"),
            },
            {
              key: "result",
              icon: <PiExamLight />,
              label: "Result",
              onClick: () => navigate("/student/result"),
            },
            {
              key: "logout",
              icon: <LuLogOut />,
              label: "Logout",
              onClick: () => {
                localStorage.removeItem("userId");
                localStorage.removeItem("userRole");
                message.success("Logged out");
                navigate("/");
              },
            },
          ]}
        />
      </Sider>
      <Layout
        style={{
          marginLeft: 200,
        }}
      >
        <Header
          style={{
            padding: 0,
            background: "white",
            position: "sticky",
            paddingTop: 10,
            paddingRight: 10,
            top: 0,
            zIndex: 1,
            height: 60,
            display: "flex",
            justifyContent: "end",
          }}
        ></Header>
        <Content
          style={{
            margin: "20px 20px 0",
            overflow: "initial",
          }}
        >
          <div
            style={{
              padding: 20,
              textAlign: "center",
              background: colorBgContainer,
              borderRadius: borderRadiusLG,
            }}
          >
            <Spin spinning={isLoading}>
              <Outlet context={{ setIsLoading }} />
            </Spin>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};
export default AppLayout;
