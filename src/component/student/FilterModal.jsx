import { Select, Space } from "antd";
import React, { useState } from "react";
const options = [];
for (let i = 0; i < 3; i++) {
  const value = i.toString(36) + i;
  options.push({
    label: `Long Label: ${value}`,
    value,
  });
}
const sharedProps = {
  mode: "multiple",
  style: {
    width: "100%",
  },
  options,
  placeholder: "Filter",
  maxTagCount: "responsive",
};
const FilterModal = (props) => {
  const [value, setValue] = useState();
  const selectProps = {
    value,
    onChange: setValue,
  };
  return (
    <Space
      direction="vertical"
      style={{
        width: "100px",
      }}
    >
      <Select {...sharedProps} {...selectProps} />
    </Space>
  );
};
export default FilterModal;
