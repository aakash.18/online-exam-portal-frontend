import React, { useEffect, useState } from "react";

import { Space } from "antd";
import axios from "axios";
import { useNavigate, useOutletContext } from "react-router-dom";
import FilterModal from "../FilterModal";
import SearchStudentExam from "./SearchStudentExam";
import StudentExamTable from "./StudentExamTable";

const StudentExam = () => {
  const navigate = useNavigate();
  const { setIsLoading } = useOutletContext();
  const [examList, setExamList] = useState();
  const [categoryPassingMarksList, setCategoryPassinMarksList] = useState();

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`${"http://localhost:8899"}/exam/all/${localStorage.getItem('userId')}`)
      .then((response) => {
        setExamList(response.data.data);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return (
    <>
      <div className="container">
        <span className="span">Exams</span>
       
      </div>

      {examList && (
        <StudentExamTable
          examList={examList}
          setExamList={setExamList}
          categoryPassingMarksList={categoryPassingMarksList}
          setCategoryPassinMarksList={setCategoryPassinMarksList}
        />
      )}
    </>
  );
};

export default StudentExam;

{
  /* <Modal
          title="Create StudentExam"
          style={{
            top: 20,
            width: 700,
          }}
          open={modalOpen}
          onOk={() => setModalOpen(false)}
          onCancel={() => setModalOpen(false)}
        >
          <UpdateStudentExam />
        </Modal> */
}
