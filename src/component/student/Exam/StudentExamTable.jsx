import { Button, Table, Tooltip } from "antd";
import dayjs from "dayjs";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Text } from "recharts";

const StudentExamTable = ({ examList, setExamList }) => {
  const navigate = useNavigate();
  console.log("examlist length: ", examList.length);
  const [currentRow, setCurrentRow] = useState();

  const columns = [
    {
      title: "Sr no",
      dataIndex: "srno",
      width: 60,
    },
    {
      title: "Exam Name",
      dataIndex: "examname",
      ellipsis: {
        showTitle: false,
      },
      render: (question) => (
        <Tooltip placement="topLeft" title={question}>
          {question}
        </Tooltip>
      ),
    },
    {
      title: "Date",
      dataIndex: "date",
      width: 100,
    },
    {
      title: "Marks",
      dataIndex: "marks",
      width: 100,
    },
    {
      title: "Duration",
      dataIndex: "duration",
      width: 100,
    },
    {
      title: "Starting Time",
      dataIndex: "startingtime",
      width: 150,
    },
    {
      title: "Ending Time",
      dataIndex: "endingtime",
      width: 150,
    },
    {
      title: "Status",
      dataIndex: "status",
      render: (value, record) => {
        return (
          <>
            <Text
              style={{
                color: record.status === "Live" ? "green" : "",
                fontWeight: record.status === "Live" ? "bolder" : "",
              }}
            >
              {record.status}
            </Text>
          </>
        );
      },
      width: 100,
    },
    {
      title: "Action",
      dataIndex: "detail",
      render: (value, record) => {
        return (
          <>
            <Button
              className="button"
              type="primary"
              disabled={record.status !== "Live" || record.examGiven ? true : false}
              onClick={() => {
                setCurrentRow(record.key);
                navigate(`/student/exam-dashboard/${record.key}`);
              }}
            >
              {record.examGiven ? "Given" : "Give"}
            </Button>
          </>
        );
      },
    },
  ];
  const data = [];
  for (let i = 0; i < examList.length; i++) {
    data.push({
      key: examList[i].examId,
      srno: i + 1,
      duration: dayjs(examList[i].duration).format("hh:mm:ss"),
      examname: examList[i].examName,
      marks: examList[i].totalMarks,
      date: dayjs(examList[i].date).format("YYYY-MM-DD"),
      createdby: examList[i].userName,
      startingtime: dayjs(examList[i].startTime).format("hh:mm:ss A"),
      endingtime: dayjs(examList[i].endTime).format("hh:mm:ss A"),
      status:
        dayjs(examList[i].date).format("YYYY-MM-DD").toString() ===
        dayjs().format("YYYY-MM-DD").toString()
          ? "Live"
          : dayjs(examList[i].date).isAfter(dayjs())
          ? "Upcoming"
          : "Completed",
      
      noOfLogicalQuestion: examList[i].noOfLogicalQuestion,
      noOfTechnicalQuestion: examList[i].noOfTechnicalQuestion,
      noOfProgrammingQuestion: examList[i].noOfProgrammingQuestion,
      examGiven: examList[i].examGiven,
    });
  }

  return (
    <>
      <Table
        pagination={false}
        columns={columns}
        size="middle"
        dataSource={data}
      />
    </>
  );
};
export default StudentExamTable;
