import { Button, Modal, Space, Table, Tooltip } from "antd";
import dayjs from "dayjs";
import React, { useState } from "react";
import { useNavigate, useOutletContext } from "react-router-dom";
import { Text } from "recharts";
import DeleteModall from "../DeleteModall";

const StudentResultTable = ({ resultList, setResultList }) => {
  const navigate = useNavigate();
  const [updateModal, setupdateModal] = useState(false);
  const [viewModal, setViewModal] = useState(false);
  const [DeleteModal, setDeleteModal] = useState(false);
  const { setIsLoading } = useOutletContext();

  const [currentRow, setCurrentRow] = useState();

  const columns = [
    {
      title: "Sr no",
      dataIndex: "srno",
      width: 60,
    },
    {
      title: "Exam Name",
      dataIndex: "examName",
      ellipsis: {
        showTitle: false,
      },
      render: (question) => (
        <Tooltip placement="topLeft" title={question}>
          {question}
        </Tooltip>
      ),
    },
    {
      title: "Date",
      dataIndex: "date",
    },
    {
      title: "Total Marks",
      dataIndex: "totalMarks",
    },
    {
      title: "obtained marks",
      dataIndex: "obtainedMarks",
    },
    {
      title: "Status",
      dataIndex: "status",
      render: (value, record) => {
        return (
          <>
            <Text
              style={
                record.status === "FAIL"
                  ? { color: "red", fontWeight: "bolder" }
                  : { color: "green", fontWeight: "bolder" }
              }
            >
              {record.status}
            </Text>
          </>
        );
      },
    },

    {
      title: "Action",

      dataIndex: "detail",
      render: (value, record) => {
        return (
          <>
            <Button
              className="button"
              type="primary"
              onClick={() => {
                setCurrentRow(record.key);
                navigate(`/student/result-detail/${record.key}`);
              }}
            >
              View
            </Button>
          </>
        );
      },
    },
  ];
  const data = [];
  {
    console.log("result list ", resultList);
  }
  for (let i = 0; i < resultList.length; i++) {
    data.push({
      key: resultList[i].responseExamDto.examId,
      srno: i + 1,
      userName: resultList[i].responseResultDto.userName,
      date: dayjs(resultList[i].responseExamDto.date).format("YYYY/MM/DD"),
      logicalMarks: resultList[i].responseResultDto.logicalMarks,
      tehnicalMarks: resultList[i].responseResultDto.technicalMarks,
      programmingMarks: resultList[i].responseResultDto.programmingMarks,

      logicalStatus: resultList[i].responseResultDto.logicalStatus,
      technicalStatus: resultList[i].responseResultDto.technicalStatus,
      programmingStatus: resultList[i].responseResultDto.programmingStatus,

      obtainedMarks:
        resultList[i].responseResultDto.logicalMarks +
        resultList[i].responseResultDto.technicalMarks +
        resultList[i].responseResultDto.programmingMarks,
      status:
        resultList[i].responseResultDto.status === false ? "FAIL" : "PASS",
      examName: resultList[i].responseExamDto.examName,
      examDate: dayjs(resultList[i].responseExamDto.date).format("YYYY-MM-DD"),
      adminName: resultList[i].responseExamDto.userName,
      totalMarks: resultList[i].responseExamDto.totalMarks,
    });
  }

  return (
    <>
      <Table
        pagination={false}
        columns={columns}
        size="middle"
        dataSource={data}
      />
    </>
  );
};
export default StudentResultTable;
