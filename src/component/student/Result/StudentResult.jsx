import React, { useEffect, useState } from "react";

import { Space, message } from "antd";
import axios from "axios";
import { useOutletContext, useParams } from "react-router-dom";
import FilterModal from "../FilterModal";
import StudentResultTable from "./StudentResultTable";

const StudentResult = () => {
  const params = useParams();
  const { setIsLoading } = useOutletContext();
  const [resultList, setResultList] = useState();

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(
        `${"http://localhost:8899"}/result/all/${localStorage.getItem(
          "userId"
        )}`
      )
      .then((response) => {
        console.log(response.data.data);
        setResultList(response.data.data);
      })
      .catch((error) => {
        console.log(error.response.data.message);
        message.error(error.response.data.message);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return (
    <div>
      <div className="container">
        <span className="span">Results</span>
        
      </div>
      {resultList && (
        <StudentResultTable
          resultList={resultList}
          setResultList={setResultList}
        />
      )}
    </div>
  );
};

export default StudentResult;
