import { Button, Col, Row, Spin } from "antd";
import { Header } from "antd/es/layout/layout";
import axios from "axios";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import "./StudentExamDashboard";
const StudentExamDashboard = () => {
  const params = useParams();
  const [exam, setExam] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`${"http://localhost:8899"}/exam/${params.examId}`, {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      })
      .then((response) => {
        setExam(response.data.data);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  const navigate = useNavigate();
  return (
    <div>
      <Header
        style={{
          padding: 0,
          background: "white",
          position: "sticky",
          paddingTop: 10,
          paddingRight: 10,
          top: 0,
          zIndex: 1,
          height: 60,
          display: "flex",
          justifyContent: "end",
        }}
      ></Header>

      {isLoading ? (
        <Spin spinning={isLoading} />
      ) : (
        <div>
          <h4 className="container">Exam Detail</h4>
          <div className="gap" />
          <Row className="container">
            <Col span={4}>Exam Title</Col>
            <Col span={4} style={{ fontWeight: "bold" }}>
              {exam.examName}
            </Col>
            <Col span={16} style={{ fontWeight: "bold" }}>
              Exam Description
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Subject</Col>
            <Col span={4} style={{ fontWeight: "bold" }}>
              {exam.examName}
            </Col>
            <Col span={16}>
              The best way to avoid technical issues during the Proctored Exam
              is to complete the onboarding process with the exact setup you
              will use to take the Proctored Exam. Please also be aware that
              previous onboarding approval is valid for a finite period of time.
              If you have any concerns about whether your onboarding approval
              will last throughout your Proctored Exam, you should re-do the
              Onboarding Exam
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Exam Duration</Col>
            <Col span={20} style={{ fontWeight: "bold" }}>
              {dayjs(exam.duration).format("hh:mm:ss")}
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Date</Col>
            <Col span={20} style={{ fontWeight: "bold" }}>
              {exam.date}
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Created By</Col>
            <Col span={20} style={{ fontWeight: "bold" }}>
              {exam.userName}
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Exam Time Slot</Col>
            <Col span={20} style={{ fontWeight: "bold" }}>
              {dayjs(exam.startTime).format("hh:mm:ss A")} -
              {dayjs(exam.endTime).format("hh:mm:ss A")}
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Number of Question</Col>
            <Col span={20} style={{ fontWeight: "bold" }}>
              {exam.noOfLogicalQuestion +
                exam.noOfProgrammingQuestion +
                exam.noOfTechnicalQuestion}
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Logical Passing marks</Col>
            <Col span={20} style={{ fontWeight: "bold" }}>
              {exam.logicalPassingMarks} Marks
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Technical Passing marks</Col>
            <Col span={8} style={{ fontWeight: "bold" }}>
              {exam.technicalPassingMarks} Marks
            </Col>
            <Col span={3} style={{ fontWeight: "bold" }}>
              <Button
                type="primary"
                style={{
                  width: "130px",
                  background: "black",
                  borderColor: "yellow",
                }}
                onClick={() => navigate("/student/exam")}
              >
                Cancel
              </Button>
            </Col>
            <Col span={9} style={{ fontWeight: "bold" }}>
              <Button
                type="primary"
                style={{ width: "130px" }}
                onClick={() => {
                  navigate(`/student/exam/${params.examId}`);
                }}
              >
                Start
              </Button>
            </Col>
          </Row>
          <div className="small-gap" />
          <Row className="container">
            <Col span={4}>Programming Passing marks</Col>
            <Col span={20} style={{ fontWeight: "bold" }}>
              {exam.programmingPassingMarks} Marks
            </Col>
          </Row>
          <div className="small-gap" />
        </div>
      )}
    </div>
  );
};

export default StudentExamDashboard;
