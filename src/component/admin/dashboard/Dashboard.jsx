import { Card, Col, Divider, Row } from "antd";
import Meta from "antd/es/card/Meta";
import React, { useEffect, useState } from "react";
import MyCard from "../dashboard/MyCard.css";
import { Pie } from "@ant-design/plots";
import axios from "axios";
import { useOutletContext } from "react-router-dom";

const Dashboard = () => {
  const { setIsLoading } = useOutletContext();
  const [adminDashboardDetail, setAdminDashboardDetail] = useState();
  const [config, setConfig] = useState();

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`${"http://localhost:8899"}/user/admin-dashboard/`)
      .then((response) => {
        console.log(response.data.data);
        setAdminDashboardDetail(response.data.data);
        const data = [
          {
            type: "Logical Questions",
            value: response.data.data.totalLogicalQuestions,
          },
          {
            type: "Programming Questions",
            value: response.data.data.totalProgrammingQuestions,
          },
          {
            type: "Technical Questions",
            value: response.data.data.totalTechnicalQuestions,
          },
        ];
        setConfig({
          appendPadding: 10,
          data,
          angleField: "value",
          colorField: "type",
          radius: 0.9,

          tooltip: {
            fields: ["type", "value"],
            formatter: (data) => {
              console.log("data: ", data);
              return { name: data.type, value: data.value };
            },
            title: "type",
          },
        });
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return (
    <>
      {adminDashboardDetail && (
        <div>
          <Row>
            <Col span={5}>
              <h4>Welcome Admin!</h4>
            </Col>
          </Row>

          <Row style={{ marginTop: "20px" }}>
            <Col span={8}>
              <Card
                hoverable
                style={{ width: 280, height: 100, background: "#6943ce" }}
              >
                <Meta
                  title={adminDashboardDetail.totalStudents}
                  description="Total Students"
                />
              </Card>
            </Col>
            <Col span={8}>
              <Card
                hoverable
                style={{ width: 280, height: 100, background: "#6943ce" }}
              >
                <Meta
                  title={adminDashboardDetail.totalExams}
                  description="Total Exams"
                />
              </Card>
            </Col>

            <Col span={8}>
              <Card
                hoverable
                style={{ width: 280, height: 100, background: "#6943ce" }}
              >
                <Meta
                  title={adminDashboardDetail.totalQuestions}
                  description="Total Questions"
                />
              </Card>
            </Col>
            <Divider />
          </Row>
          <Row>
            <Col span={24} style={{ textAlign: "start" }}>
              <h4>Questions by Categories</h4>
            </Col>
          </Row>
          <Pie {...config} height={400} width={500} />
        </div>
      )}
    </>
  );
};

export default Dashboard;
