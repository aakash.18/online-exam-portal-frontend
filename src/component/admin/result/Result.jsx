import { Space } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  useNavigate,
  useOutletContext
} from "react-router-dom";
import FilterModal from "../FilterModal";
import ResultTable from "./ResultTable";
import SearchResult from "./SearchResult";

const Result = () => {
  const navigate = useNavigate();
  const { setIsLoading } = useOutletContext();
  const [examList, setExamList] = useState();
  const [categoryPassingMarksList, setCategoryPassinMarksList] = useState();

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`${"http://localhost:8899"}/exam/all/`)
      .then((response) => {
        setExamList(response.data.data);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return (
    <>
      <div className="container">
        <Space direction="horizontal" align="start">
          <span className="span">Results</span>
        </Space>
      </div>
      {examList && (
        <ResultTable
        examList={examList}
        setExamList={setExamList}
        categoryPassingMarksList={categoryPassingMarksList}
        setCategoryPassinMarksList={setCategoryPassinMarksList}
        />
      )}
    </>
  );
};

export default Result;
