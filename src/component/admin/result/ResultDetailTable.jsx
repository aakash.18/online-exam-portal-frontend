import { Button, Modal, Select, Space, Table } from "antd";
import dayjs from "dayjs";
import React, { useState } from "react";
import { useOutletContext, useParams } from "react-router-dom";
import { Text } from "recharts";
import FilterModal from "../FilterModal";
import SearchBar from "../SearchBar";
import AdmitCard from "./AdmitCardDetail";

const ResultDetailTable = ({ examDetail, resultList, setResultList, handleFilterChange }) => {
  const params = useParams();
  const [updateModal, setupdateModal] = useState(false);
  const [currentRow, setCurrentRow] = useState();
  const { setIsLoading } = useOutletContext();

  const columns = [
    {
      title: "Sr no",
      dataIndex: "srno",
      width: 60,
    },
    {
      title: "User Name",
      dataIndex: "userName",
    },
    {
      title: "Obtained Marks",
      dataIndex: "obtainedmarks",
    },
    {
      title: "Status",
      dataIndex: "status",
      render: (value, record) => {
        return (
          <>
            <Text
              style={
                record.status === "FAIL"
                  ? { color: "red", fontWeight: "bolder" }
                  : { color: "green", fontWeight: "bolder" }
              }
            >
              {record.status}
            </Text>
          </>
        );
      },
    },

    {
      title: "Action",
      dataIndex: "action",
      render: (value, record) => {
        return (
          <>
            <Button
              className="button"
              type="primary"
              onClick={() => {
                setCurrentRow(record.key);
                setupdateModal(true);
                // navigate("final-result")
              }}
            >
              View
            </Button>
          </>
        );
      },
    },
  ];

  const data = [];
  for (let i = 0; i < resultList.length; i++) {
    data.push({
      key: resultList[i].responseResultDto.resultId,
      srno: i + 1,
      userName: resultList[i].responseResultDto.userName,

      logicalMarks: resultList[i].responseResultDto.logicalMarks,
      tehnicalMarks: resultList[i].responseResultDto.technicalMarks,
      programmingMarks: resultList[i].responseResultDto.programmingMarks,

      logicalStatus: resultList[i].responseResultDto.logicalStatus,
      technicalStatus: resultList[i].responseResultDto.technicalStatus,
      programmingStatus: resultList[i].responseResultDto.programmingStatus,

      obtainedmarks:
        resultList[i].responseResultDto.logicalMarks +
        resultList[i].responseResultDto.technicalMarks +
        resultList[i].responseResultDto.programmingMarks,
      status:
        resultList[i].responseResultDto.status === false ? "FAIL" : "PASS",
      examName: resultList[i].responseExamDto.examName,
      examDate: dayjs(resultList[i].responseExamDto.date).format("YYYY-MM-DD"),
      adminName: resultList[i].responseExamDto.userName,
      totalMarks: resultList[i].responseExamDto.totalMarks,
    });
  }

  return (
    <>
      <div className="container">
        {data && (
          <Space direction="vertical" align="start">
            <span className="span">Result Detail</span>
            <span
              style={{ fontWeight: "lighter", color: "grey" }}
              className="span"
            >
              {examDetail.examName} - {examDetail.examDate}
            </span>
            <span
              style={{ fontWeight: "lighter", color: "grey" }}
              className="span"
            >
              Created By: {examDetail.adminName} | Marks : {examDetail.totalMarks}
            </span>
          </Space>
        )}
        <Space direction="horizontal" align="end">
          <Select
            onChange={(value) => {
              handleFilterChange(value);
            }}
            defaultValue="all"
            style={{ width: 120 }}
          >
            <Select.Option value="pass">Pass</Select.Option>
            <Select.Option value="fail">Fail</Select.Option>
            <Select.Option value="all">All</Select.Option>
          </Select>
        </Space>
      </div>
      <Table
        pagination={false}
        columns={columns}
        size="middle"
        dataSource={data}
      />
      <Modal
        title="Admit Card "
        style={{
          top: 100,
          width: 700,
        }}
        open={updateModal}
        onCancel={() => setupdateModal(false)}
        footer={null}
      >
        {resultList && (
          <AdmitCard
            result={resultList.find(
              (result) => result.responseResultDto.resultId === currentRow
            )}
          />
        )}
      </Modal>
    </>
  );
};
export default ResultDetailTable;
