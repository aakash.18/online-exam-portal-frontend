import { message } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useOutletContext, useParams } from "react-router-dom";
import ResultDetailTable from "./ResultDetailTable";

const ResultDetail = () => {
  const [examDetail, setExamDetail] = useState();
  const [resultList, setResultList] = useState();
  const params = useParams();
  const { setIsLoading } = useOutletContext();

  const handleFilterChange = (value) => {
    if (value === "all") {
      setIsLoading(true);
      axios
        .post(`${"http://localhost:8899"}/result/status`, {
          status: true,
          examId: params.examId,
        })
        .then((response) => {
          setExamDetail(response.data.data.responseExamDto);
          setResultList(response.data.data.responseResultDetailDtoList);
        })
        .catch((error) => {
          message.error(error.response.data.message);
        })
        .finally(() => {
          setIsLoading(false);
        });
    } else {
      setIsLoading(true);
      axios
        .post(`${"http://localhost:8899"}/result/result-status`, {
          status: value === "pass" ? true : false,
          examId: params.examId,
        })
        .then((response) => {
          setExamDetail(response.data.data.responseExamDto);
          setResultList(response.data.data.responseResultDetailDtoList);
        })
        .catch((error) => {
          message.error(error.response.data.message);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  };

  useEffect(() => {
    setIsLoading(true);
    axios
      .post(`${"http://localhost:8899"}/result/status`, {
        status: true,
        examId: params.examId,
      })
      .then((response) => {
        setExamDetail(response.data.data.responseExamDto);
        setResultList(response.data.data.responseResultDetailDtoList);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);
  return (
    <div>
      {resultList && (
        <ResultDetailTable
          examDetail={examDetail}
          resultList={resultList}
          setResultList={setResultList}
          handleFilterChange={handleFilterChange}
        />
      )}
    </div>
  );
};

export default ResultDetail;
