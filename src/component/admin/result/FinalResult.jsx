import { Button, Space } from "antd";
import React from "react";
import { MdDownload } from "react-icons/md";
import AdmitCardDetail from "./AdmitCardDetail";

const FinalResult = () => {
  return (
    <div>
      <div className="container">
        <Space direction="horizontal" align="start">
          <span className="span">Admit Card</span>
        </Space>
        <Space direction="horizontal" align="end">
          <Button type="primary" icon={<MdDownload />}>
            Download
          </Button>
        </Space>
      </div>
      <div className="container" style={{ marginTop: 20 }} />
      <AdmitCardDetail />
    </div>
  );
};

export default FinalResult;
