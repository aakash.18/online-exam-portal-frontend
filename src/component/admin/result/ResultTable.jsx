import { Button, Table } from "antd";
import dayjs from "dayjs";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Text } from "recharts";

const ExamTable = ({
  setCategoryPassingMarksList,
  categoryPassingMarksList,
  examList,
  setExamList,
}) => {
  const navigate = useNavigate();
  const [currentRow, setCurrentRow] = useState();

  const columns = [
    {
      title: "Sr no",
      dataIndex: "srno",
      width: 60,
    },
    {
      title: "Exame Name",
      dataIndex: "examname",
    },
    {
      title: "Total Marks",
      dataIndex: "totalmarks",
      width: 100,
    },
    {
      title: "Date",
      dataIndex: "date",
    },

    {
      title: "Created By",
      dataIndex: "createdby",
    },

    {
      title: "Exam Status",
      dataIndex: "examStatus",
      render: (value, record) => {
        return (
          <>
            <Text
              style={{
                color: record.examStatus === "Live" ? "green" : "",
                fontWeight: record.examStatus === "Live" ? "bolder" : "",
              }}
            >
              {record.examStatus}
            </Text>
          </>
        );
      },
    },
    {
      title: "Show",
      dataIndex: "show",
      render: (value, record) => {
        return (
          <Button
            style={{
              borderColor: "green",
              background: "green",
              color: "white",
            }}
            className="button"
            type="primary"
            onClick={() => {
              setCurrentRow(record.key);
              navigate(`/admin/all-result-detail/${record.key}`);
            }}
          >
            Show
          </Button>
        );
      },
    },
  ];
  const data = [];
  for (let i = 0; i < examList.length; i++) {
    data.push({
      key: examList[i].examId,
      srno: i + 1,
      examname: examList[i].examName,
      totalmarks: examList[i].totalMarks,
      date: dayjs(examList[i].date).format("YYYY-MM-DD"),
      createdby: examList[i].userName,
      examStatus:
        dayjs(examList[i].date).format("YYYY-MM-DD").toString() ===
        dayjs().format("YYYY-MM-DD").toString()
          ? "Live"
          : dayjs(examList[i].date).isAfter(dayjs())
          ? "Upcoming"
          : "Completed",
      noOfLogicalQuestion: examList[i].noOfLogicalQuestion,
      noOfTechnicalQuestion: examList[i].noOfTechnicalQuestion,
      noOfProgrammingQuestion: examList[i].noOfProgrammingQuestion,
    });
  }

  return (
    <>
      <Table
        pagination={false}
        columns={columns}
        size="middle"
        dataSource={data}
      />
    </>
  );
};
export default ExamTable;
