import { Input, Space, Table } from "antd";
import React from "react";

const { TextArea } = Input;
const AdmitCard = ({ result }) => {
  console.log("result: ", result);
  const columns = [
    {
      title: "Logical ",
      dataIndex: "logical",
      width: 400,
      children: [
        {
          title: "Marks",
          dataIndex: "logicalMarks",
        },
        {
          title: "Status",
          dataIndex: "logicalStatus",
        },
      ],
    },
    {
      title: "Technical ",
      dataIndex: "technical",
      children: [
        {
          title: "Marks",
          dataIndex: "tehnicalMarks",
        },
        {
          title: "Status",
          dataIndex: "technicalStatus",
        },
      ],
    },
    {
      title: "Programming ",
      dataIndex: "programming",
      children: [
        {
          title: "Marks",
          dataIndex: "programmingMarks",
        },
        {
          title: "Status",
          dataIndex: "programmingStatus",
        },
      ],
    },
  ];

  const tableData = [];
  tableData.push({
    logicalMarks: result.responseResultDto.logicalMarks,
    tehnicalMarks: result.responseResultDto.technicalMarks,
    programmingMarks: result.responseResultDto.programmingMarks,

    logicalStatus: result.responseResultDto.logicalStatus ? "PASS" : "FAIL",
    technicalStatus: result.responseResultDto.technicalStatus ? "PASS" : "FAIL",
    programmingStatus: result.responseResultDto.programmingStatus
      ? "PASS"
      : "FAIL",
  });
  return (
    <div>
      <Space direction="horizontal" align="start">
        <Space direction="vertical" align="start">
          <Space direction="horizontal" align="start">
            <span style={{ fontWeight: "bold" }}> Name:</span>
            <span>{result.responseResultDto.userName}</span>
            <div />
          </Space>
          <Space direction="horizontal" align="start">
            <span style={{ fontWeight: "bold" }}>User Name:</span>
            <span>{result.responseResultDto.userName}</span>
            <div />
          </Space>
          <Space direction="horizontal" align="start">
            <span style={{ fontWeight: "bold" }}>Exam Name:</span>
            <span>{result.responseExamDto.examName}</span>
            <div />
          </Space>
          <div style={{ height: 20 }} />
        </Space>
      </Space>
      <Table
        columns={columns}
        dataSource={tableData}
        size="middle"
        pagination={false}
      />
      <Space direction="horizontal" align="start">
        <span style={{ fontWeight: "bold" }}>Obtained Marks</span>
        <span>
          {result.responseResultDto.logicalMarks +
            result.responseResultDto.technicalMarks +
            result.responseResultDto.programmingMarks}
        </span>
        <div />
      </Space>
      <Space direction="horizontal" align="start" style={{paddingTop: "16px", paddingBottom: "16px"}}>
        <span style={{ fontWeight: "bold" }}>Final Status</span>
        <span
          style={
            result.responseResultDto.status
              ? { fontWeight: "bold", color: "green" }
              : { fontWeight: "bold", color: "red" }
          }
        >
          {result.responseResultDto.status ? "PASS" : "FAIL"}
        </span>
        <div />
      </Space>
    </div>
  );
};
export default AdmitCard;
