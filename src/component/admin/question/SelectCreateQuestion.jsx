import React from "react";
import { Card, Space } from "antd";
import { useNavigate } from "react-router-dom";
const { Meta } = Card;

const SelectCreateQuestion = () => {
    const navigate = useNavigate();
  return (

    <div>
      <Space direction="horizontal" align="start">
        <Card
          hoverable
          style={{
            width: 240,
            padding:40,
            marginRight:40
          }}
          onClick={() => navigate("/admin/createquestion")}
          cover={
            <img alt="example" src="https://logodix.com/logo/634154.png" />
          }
        >
          <Meta
            title="MCQ questions"
            description="Logical/Technical Questions"
          />
        </Card>
        <Card
          hoverable
          style={{
            width: 240,
            padding:40,
            marginLeft:40
          }}
          onClick={() => navigate("/admin/programmingquestion")}
          cover={
            <img alt="example" src="https://th.bing.com/th/id/R.fecb4080c170df1d974e6758ba26bfa5?rik=Gn0jRpyYsVJFwg&riu=http%3a%2f%2fpluspng.com%2fimg-png%2fcoder-png-source-code-icon-1600.png&ehk=VXqs7uL%2bZCLoMLGKZNAWx7hnZ%2fUBd%2b41YTXpCM4hT9A%3d&risl=&pid=ImgRaw&r=0" />
          }
        >
          <Meta
            title="CODE"
            description="Programming Question"
          />
        </Card>
      </Space>
    </div>
  );
};

export default SelectCreateQuestion;
