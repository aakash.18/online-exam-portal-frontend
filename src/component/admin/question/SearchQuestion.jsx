import React, { Component } from "react";
import { Select } from "antd";
import { SearchOutlined } from "@ant-design/icons";
const onChange = (value) => {
  console.log(`selected ${value}`);
};
const onSearch = (value) => {
  console.log("search:", value);
};

// Filter `option.label` match the user type `input`

const filterOption = (input, option) =>
  (option?.label ?? "").toLowerCase().includes(input.toLowerCase());

const SearchQuestion = ({ questionList, setQuestionList }) => (
  <Select
    style={{ width: 200 }}
    suffixIcon={<SearchOutlined />}
    showSearch
    placeholder="Search Question"
    optionFilterProp="children"
    onChange={onChange}
    onSearch={onSearch}
    filterOption={filterOption}

    options={[
      {
        value: "q1",
        label: "Question 1",
      },
      {
        value: "lucy",
        label: "Lucy",
      },
      {
        value: "tom",
        label: "Tom",
      },
    ]}
  />
);
export default SearchQuestion;
