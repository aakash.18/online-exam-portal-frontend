import React, { useState } from "react";
import { PlusOutlined } from "@ant-design/icons";
import {
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Upload,
  Space,
  Row,
  Col,
} from "antd";

const { TextArea } = Input;
const normFile = (e) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};
const ViewProgrammingQuestion = ({}) => {
  return (
    <>
       <div>
      <Row className="question-span">
        <Col span={24}>{"hi"}</Col>
      </Row>
      <Row className="option-row">
        <Col span={12}>(A) {"hi"} </Col>
        <Col span={12}>(B) {"hi"}</Col>
      </Row>
      <Row className="option-row">
        <Col span={12}>(C) {"hi"} </Col>
        <Col span={12}>(D) {"hi"} </Col>
      </Row>
      <Space direction="horizontal" align="start">
        <div className="question-span">Correct Option</div>
        <div>{"hi"}</div>
      </Space>
      <div>Category : {"hi"}</div>
      <div>Marks : {"hi"}</div>
      <div>Created by: {"hi"} </div>
    </div>
    </>
  );
};
export default () => <ViewProgrammingQuestion />;
