import { Button, Form, Input, Select, Space, message } from "antd";
import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";

const ProgrammingQuestion = () => {
    const navigate = useNavigate();
    const [formRef] = Form.useForm();
    const handleSubmit = (values) => {
      axios.post(`${"http://localhost:8899"}/question`, {
        question: values.question,
        opt1: "",
        opt2: "",
        opt3: "",
        opt4: "",
        correctAns: values.correctAns,
        difficulty: values.difficulty,
        imgUrls: "",
        typeOfCategory: 5,
        adminId: localStorage.getItem("userId"),
        mark: 5
      }).then((response) => {
        console.log(response.data);
        message.success("Question added successfully");
        formRef.resetFields([]);
        navigate("/admin/question")
      }).catch((error) => {
        console.log(error.response.data.message);
        message.error(error.response.data.message);
      });
    };
  return (
    <div className="container">
      <Form
        form={formRef}
        layout="vertical"
        colon={false}
        onFinish={handleSubmit}
      >
        <Space direction="vertical" align="start">
          <h4>Add Programming Question</h4>
          <Form.Item label="Problem Statement" name="question">
            <Input.TextArea rows="2" cols="150" />
          </Form.Item>

          <Form.Item label="Difficulty Level" name="difficulty">
              <Select
                showSearch
                style={{ width: 225 }}
                placeholder="Search to Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? "").includes(input)
                }
                filterSort={(optionA, optionB) =>
                  (optionA?.label ?? "")
                    .toLowerCase()
                    .localeCompare((optionB?.label ?? "").toLowerCase())
                }
                options={[
                  {
                    value: "ADVANCE",
                    label: "Advance",
                  },
                  {
                    value: "MODERATE",
                    label: "Moderate",
                  },
                  {
                    value: "EASY",
                    label: "Easy",
                  },
                ]}
              />
            </Form.Item>
          <Form.Item label="Reference Answer Code" name="correctAns">
            <Input.TextArea rows="8" cols="150" />
          </Form.Item>
          <Space align="end">
            <Button
              htmlType="submit"
              style={{ width: 225 }}
              type="primary"
              block
            >
              Add
            </Button>
          </Space>
        </Space>
      </Form>
    </div>
  );
};

export default ProgrammingQuestion;
