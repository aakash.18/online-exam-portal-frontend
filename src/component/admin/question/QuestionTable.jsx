import React, { useState } from "react";
import {
  Button,
  Divider,
  Flex,
  Modal,
  Space,
  Table,
  Tooltip,
  message,
} from "antd";
import { Minimize } from "@mui/icons-material";
import { EyeTwoTone, EditOutlined, DeleteFilled } from "@ant-design/icons";
import { Link, Navigate, Outlet, useNavigate } from "react-router-dom";
import UpdateQuestion from "./UpdateQuestion";
import "./Question.css";
import QuestionDetail from "./QuestionDetail";
import DeleteModall from "../DeleteModall";
import axios from "axios";

const QuestionTable = ({ questionList, setQuestionList }) => {
  const navigate = useNavigate();
  const [updateModal, setupdateModal] = useState(false);
  const [viewModal, setViewModal] = useState(false);
  const [DeleteModal, setDeleteModal] = useState(false);

  const [currentRow, setCurrentRow] = useState();

  const columns = [
    {
      title: "Sr no",
      dataIndex: "srno",
      width: 60,
    },
    {
      title: "Question",
      dataIndex: "question",
      ellipsis: {
        showTitle: false,
      },
      render: (question) => (
        <Tooltip placement="topLeft" title={question}>
          {question}
        </Tooltip>
      ),
    },
    {
      title: "Correct Answer",
      dataIndex: "answer",
      width: 160,
      ellipsis: {
        showTitle: false,
      },
      render: (question) => (
        <Tooltip placement="topLeft" title={question}>
          {question}
        </Tooltip>
      ),
    },
    {
      title: "Category",
      dataIndex: "category",
    },
    {
      title: "Marks",
      dataIndex: "marks",
      width: 60,
    },
    {
      title: "Created By",
      dataIndex: "createdby",
    },
    {
      title: "Action",

      dataIndex: "detail",
      render: (value, record) => {
        return (
          <>
            <EyeTwoTone
              style={{ color: "blue" }}
              onClick={() => {
                setCurrentRow(record.key);
                setViewModal(true); 
              }}
            />
            <span style={{ padding: 15 }} />
            <DeleteFilled
              style={{ color: "red" }}
              onClick={() => {
                setCurrentRow(record.key);
                setDeleteModal(true);
              }}
            />
            <span style={{ padding: 15 }} />
            <EditOutlined
              style={{ color: "green" }}
              onClick={() => {
                setCurrentRow(record.key);
                setupdateModal(true);
              }}
            />
          </>
        );
      },
    },
  ];
  const data = [];
  for (let i = 0; i < questionList.length; i++) {
    data.push({
      key: questionList[i].questionId,
      srno: i + 1,
      question: questionList[i].question,
      category: questionList[i].typeOfCategory,
      marks: questionList[i].mark,
      answer: questionList[i].correctAns,
      createdby: questionList[i].admin,
    });
  }

  const handleDeleteQuestion = () => {
    axios
      .delete(`${"http://localhost:8899"}/question/${currentRow}`)
      .then((response) => {
        if (response.status === 200) {
          setQuestionList((prevQuestionList) => {
            return prevQuestionList.filter(
              (question) => question.questionId !== currentRow
            );
          });
          message.success("Question deleted successfully");
        }
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  return (
    <>
      <Table
        pagination={false}
        columns={columns}
        size="middle"
        dataSource={data}
      />

      <UpdateQuestion
        updateModal={updateModal}
        setUpdateModal={setupdateModal}
        setQuestionList={setQuestionList}
        questionDetail={questionList.find(
          (question) => question.questionId === currentRow
        )}
      />

      <Modal
        title="Question Detail "
        style={{
          width: 700,
        }}
        centered
        open={viewModal}
        onOk={() => setViewModal(false)}
        onCancel={() => setViewModal(false)}
        cancelButtonProps={{ style: { display: "none" } }}
        okButtonProps={{ style: { display: "none" } }}
      >
        
        <QuestionDetail
          questionDetail={questionList.find(
            (question) => question.questionId === currentRow
          )}
        />
      </Modal>

      <Modal
        title="Delete "
        centered
        style={{
          width: 700,
        }}
        open={DeleteModal}
        onOk={() => {
          handleDeleteQuestion();
          setDeleteModal(false);
        }}
        onCancel={() => setDeleteModal(false)}
        okText="Delete"
      >
        <DeleteModall value="Question" />
      </Modal>
    </>
  );
};
export default QuestionTable;
