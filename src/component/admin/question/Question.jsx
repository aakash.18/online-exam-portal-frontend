import React, { useEffect, useState } from "react";

import { Button, Space } from "antd";
import axios from "axios";
import { useNavigate, useOutletContext } from "react-router-dom";
import FilterModal from "../FilterModal";
import QuestionTable from "./QuestionTable";
import SearchQuestion from "./SearchQuestion";

const Question = () => {
  const navigate = useNavigate();

  const { setIsLoading } = useOutletContext();
  const [questionList, setQuestionList] = useState();

  useEffect(() => {
    setIsLoading(true);
    axios.get(`${"http://localhost:8899"}/question/all`).then((response) => {
      setQuestionList(response.data.data);
    });
    setIsLoading(false);
  }, []);

  return (
    <>
      <div className="container">
        <span className="span">Questions</span>
        <Space direction="horizontal" align="end">
          
          <Button
            className="button"
            type="primary"
            onClick={() => navigate("/admin/select-create-question")}
          >
            Add Question
          </Button>
        </Space>
      </div>

      {questionList && (
        <QuestionTable
          questionList={questionList}
          setQuestionList={setQuestionList}
        />
      )}
    </>
  );
};

export default Question;
