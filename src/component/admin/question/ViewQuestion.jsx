import React, { useState } from "react";
import { PlusOutlined } from "@ant-design/icons";
import {
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Upload,
  Space,
} from "antd";

const { TextArea } = Input;
const normFile = (e) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};
const ViewQuestion = () => {
  return (
    <>
      <Space direction="vertical" style={{ height: 350, overflowY: "auto" }}>
        <Form
          labelCol={{
            span: 3,
          }}
          wrapperCol={{
            span: 20,
          }}
          layout="horizontal"
          style={{
            width: 470,
          }}
        >
          <Form.Item label="Category">
            <Radio.Group>
              <Radio value="logical"> Logical </Radio>
              <Radio value="technical"> Technical </Radio>
              <Radio value="programming"> Programming </Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="Text">
            <TextArea rows={2} />
          </Form.Item>
          <Form.Item
            label="Image :"
            valuePropName="fileList"
            getValueFromEvent={normFile}
          >
            <Upload action="/upload.do" listType="picture-card" maxCount={1}>
              <button
                style={{
                  border: 0,
                  background: "none",
                }}
                type="button"
              >
                <PlusOutlined />
                <div>Image</div>
              </button>
            </Upload>
          </Form.Item>
          <Form.Item label="Option A">
            <Input />
          </Form.Item>
          <Form.Item label="Option B">
            <Input />
          </Form.Item>
          <Form.Item label="Option C">
            <Input />
          </Form.Item>
          <Form.Item label="Option D">
            <Input />
          </Form.Item>
          <Form.Item label="Answer">
            <Select>
              <Select.Option value="a">Option A</Select.Option>
              <Select.Option value="b">Option B</Select.Option>
              <Select.Option value="c">Option C</Select.Option>
              <Select.Option value="d">Option D</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item label="Difficulty">
            <Select>
              <Select.Option value="easy">EASY</Select.Option>
              <Select.Option value="moderate">MODERATE</Select.Option>
              <Select.Option value="advance">ADVANCE</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item label="Date">
            <DatePicker />
          </Form.Item>
          <Form.Item label="Mark">
            <InputNumber min={1} defaultValue={1} />
          </Form.Item>
        </Form>
      </Space>
    </>
  );
};
export default ViewQuestion ;
