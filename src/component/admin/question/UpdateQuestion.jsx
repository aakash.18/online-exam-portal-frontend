import { PlusOutlined } from "@ant-design/icons";
import {
  Form,
  Input,
  InputNumber,
  Modal,
  Select,
  Space,
  Upload,
  message,
} from "antd";
import axios from "axios";
import React, { useEffect } from "react";
import "./UpdateQuestion.css";

const { TextArea } = Input;
const normFile = (e) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};
const UpdateQuestion = ({
  updateModal,
  setUpdateModal,
  questionDetail,
  setQuestionList,
}) => {
  const [formRef] = Form.useForm();

  useEffect(() => {
    //TODO: Remove static IDs
    if (!questionDetail) return;
    formRef.setFieldsValue(questionDetail);
    if (questionDetail.typeOfCategory === "Logical") {
      formRef.setFieldValue("typeOfCategory", 4);
    } else if (questionDetail.typeOfCategory === "technical") {
      formRef.setFieldValue("typeOfCategory", 5);
    } else {
      formRef.setFieldValue("typeOfCategory", 3);
    }
  });

  const handleSubmit = () => {
    const values = formRef.getFieldsValue();
    const updatedQuestion = {
      question: values.question,
      opt1: values.opt1,
      opt2: values.opt2,
      opt3: values.opt3,
      opt4: values.opt4,
      correctAns: values.correctAns,
      difficulty: values.difficulty,
      imgUrls: "",
      typeOfCategory: Number(values.typeOfCategory),
      adminId: localStorage.getItem("userId"),
      mark: values.mark,
    };
    axios
      .put(
        `${"http://localhost:8899"}/question/${questionDetail.questionId}`,
        updatedQuestion
      )
      .then((response) => {
        message.success("Question updated successfully");
        formRef.resetFields([]);
        setQuestionList((prevQuestionList) => {
          return prevQuestionList.map((question) => {
            return question.questionId === questionDetail.questionId
              ? response.data.data
              : question;
          });
        });
        setUpdateModal(false);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  return (
    <>
      <Modal
        title="Edit"
        style={{
          width: 700,
        }}
        centered
        open={updateModal}
        onOk={handleSubmit}
        onCancel={() => {
          formRef.resetFields([]);
          setUpdateModal(false);
        }}
      >
        <Space direction="vertical" style={{ height: 350, overflowY: "auto" }}>
          <Form
            form={formRef}
            labelAlign="left"
            colon={false}
            labelCol={{
              span: 4,
            }}
            wrapperCol={{
              span: 19,
            }}
            layout="horizontal"
            style={{
              width: 470,
            }}
          >
            <Form.Item name="typeOfCategory" label="Category">
              <Select
                options={[
                  {
                    value: 4,
                    label: "Logical",
                  },
                  {
                    value: 3,
                    label: "Technical",
                  },
                
                ]}
              ></Select>
            </Form.Item>

            <Form.Item name="question" label="Question">
              <TextArea rows={2} />
            </Form.Item>
            <Form.Item
              label="Image"
              valuePropName="fileList"
              getValueFromEvent={normFile}
            >
              <Upload action="/upload.do" listType="picture-card" maxCount={1}>
                <button
                  style={{
                    border: 0,
                    background: "none",
                  }}
                  type="button"
                >
                  <PlusOutlined />
                  <div>Image</div>
                </button>
              </Upload>
            </Form.Item>
            <Form.Item
              name="opt1"
              label="Option A"
              className="full-width-input"
            >
              <Input size="small" />
            </Form.Item>
            <Form.Item name="opt2" label="Option B">
              <Input size="small" />
            </Form.Item>
            <Form.Item name="opt3" label="Option C">
              <Input size="small" />
            </Form.Item>
            <Form.Item name="opt4" label="Option D">
              <Input size="small" />
            </Form.Item>
            <Form.Item name="correctAns" label="Answer">
              <Select>
                <Select.Option value="a">Option A</Select.Option>
                <Select.Option value="b">Option B</Select.Option>
                <Select.Option value="c">Option C</Select.Option>
                <Select.Option value="d">Option D</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item name="difficulty" label="Difficulty">
              <Select>
                <Select.Option value="EASY">EASY</Select.Option>
                <Select.Option value="MODERATE">MODERATE</Select.Option>
                <Select.Option value="ADVANCE">ADVANCE</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item name="mark" label="Mark">
              <InputNumber min={1} defaultValue={1} />
            </Form.Item>
          </Form>
        </Space>
        {/* </div> */}
      </Modal>
    </>
  );
};
export default UpdateQuestion;
