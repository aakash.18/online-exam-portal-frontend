import { Col, Row, Space } from "antd";
import React from "react";
import "./QuestionDetail.css";

const QuestionDetail = ({ questionDetail }) => {
  console.log(questionDetail.typeOfCategory);
  return (
    <div>
      <Row className="question-span">
        <Col span={24}>{questionDetail.question}</Col>
      </Row>
      {questionDetail.typeOfCategory !== "Programming" && (
        <Row className="option-row">
          <Col span={12}>(A) {questionDetail.opt1} </Col>
          <Col span={12}>(B) {questionDetail.opt2}</Col>
        </Row>
      )}
      {questionDetail.typeOfCategory !== "Programming" && (
        <Row className="option-row">
          <Col span={12}>(C) {questionDetail.opt3} </Col>
          <Col span={12}>(D) {questionDetail.opt4} </Col>
        </Row>
      )}
      {questionDetail.typeOfCategory !== "Programming" && (
        <Space direction="horizontal" align="start">
          <div>Correct Option</div>
          <div className="question-span">
            {questionDetail.correctAns.toUpperCase()}
          </div>
        </Space>
      )}
      <div>Category : {questionDetail.typeOfCategory}</div>
      <div>Marks : {questionDetail.mark}</div>
      <div>Created by: {questionDetail.admin} </div>
    </div>
  );
};

export default QuestionDetail;
