import { PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input, Select, Space, Upload, message } from "antd";
import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";
import "./CreateQuestion.css";

const { TextArea } = Input;

const CreateQuestion = () => {
  const navigate = useNavigate();
  const [formRef] = Form.useForm();
  const handleSubmit = (values) => {
    axios
      .post(`${"http://localhost:8899"}/question`, {
        question: values.question,
        opt1: values.optionA,
        opt2: values.optionB,
        opt3: values.optionC,
        opt4: values.optionD,
        correctAns: values.correctAns,
        difficulty: values.difficultyLevel,
        imgUrls: "",
        typeOfCategory: values.category,
        adminId: localStorage.getItem("userId"),
        mark: 1,
      })
      .then((response) => {
        console.log(response.data);
        message.success("Question added successfully");
        formRef.resetFields([]);
        navigate("/admin/question");
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };
  return (
    <div className="container">
      <Form
        form={formRef}
        layout="vertical"
        colon={false}
        onFinish={handleSubmit}
      >
        <Space direction="vertical" align="start">
          <h4>Add Question</h4>
          <Form.Item label="Question" name="question">
            <Input.TextArea rows="2" cols="150" />
          </Form.Item>
          <Space direction="horizontal" align="start">
            <Form.Item label="Option A" name="optionA">
              <Input.TextArea rows="1" cols="65" />
            </Form.Item>
            <Form.Item label="Option B" name="optionB">
              <Input.TextArea rows="1" cols="65" />
            </Form.Item>
          </Space>
          <Space direction="horizontal" align="start">
            <Form.Item label="Option C" name="optionC">
              <Input.TextArea rows="1" cols="65" />
            </Form.Item>
            <Form.Item label="Option D" name="optionD">
              <Input.TextArea rows="1" cols="65" />
            </Form.Item>
          </Space>
          <Space direction="horizontal" align="start">
            <Form.Item label="Category" name="category">
              <Select
                showSearch
                style={{ width: 225 }}
                placeholder="Search to Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? "").includes(input)
                }
                filterSort={(optionA, optionB) =>
                  (optionA?.label ?? "")
                    .toLowerCase()
                    .localeCompare((optionB?.label ?? "").toLowerCase())
                }
                options={[
                  {
                    value: "4",
                    label: "Logical",
                  },
                  {
                    value: "3",
                    label: "Technical",
                  },
                ]}
              />
            </Form.Item>

            <Form.Item label="Correct Answer" name="correctAns">
              <Select
                showSearch
                style={{ width: 225 }}
                placeholder="Search to Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? "").includes(input)
                }
                filterSort={(optionA, optionB) =>
                  (optionA?.label ?? "")
                    .toLowerCase()
                    .localeCompare((optionB?.label ?? "").toLowerCase())
                }
                options={[
                  {
                    value: "a",
                    label: "Option A",
                  },
                  {
                    value: "b",
                    label: "Option B",
                  },
                  {
                    value: "c",
                    label: "Option C",
                  },
                  {
                    value: "d",
                    label: "Option D",
                  },
                ]}
              />
            </Form.Item>

            <Form.Item label="Difficulty Level" name="difficultyLevel">
              <Select
                showSearch
                style={{ width: 225 }}
                placeholder="Search to Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? "").includes(input)
                }
                filterSort={(optionA, optionB) =>
                  (optionA?.label ?? "")
                    .toLowerCase()
                    .localeCompare((optionB?.label ?? "").toLowerCase())
                }
                options={[
                  {
                    value: "ADVANCE",
                    label: "Advance",
                  },
                  {
                    value: "MODERATE",
                    label: "Moderate",
                  },
                  {
                    value: "EASY",
                    label: "Easy",
                  },
                ]}
              />
            </Form.Item>
          </Space>
          <span>Upload Image</span>
          <Space align="end" direction="horizontal">
            <Upload action="/upload.do" listType="picture-card">
              <button
                style={{
                  border: 0,
                  background: "none",
                }}
                type="button"
              >
                <PlusOutlined />
                <div
                  style={{
                    marginTop: 8,
                  }}
                >
                  Upload
                </div>
              </button>
            </Upload>
          </Space>
          <Space align="end">
            <Button
              htmlType="submit"
              style={{ width: 225 }}
              type="primary"
              block
            >
              Add
            </Button>
          </Space>
        </Space>
      </Form>
    </div>
  );
};

export default CreateQuestion;
