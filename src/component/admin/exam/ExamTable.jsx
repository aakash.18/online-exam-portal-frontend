import { DeleteFilled, EditOutlined, EyeTwoTone } from "@ant-design/icons";
import { Button, Modal, Table } from "antd";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import DeleteModall from "../DeleteModall";
import ExamDetail from "./ExamDetail";
import UpdateExam from "./UpdateExam";
import axios from "axios";
import dayjs from "dayjs";
import { Text } from "recharts";

const ExamTable = ({
  setCategoryPassingMarksList,
  categoryPassingMarksList,
  examList,
  setExamList,
}) => {
  const navigate = useNavigate();
  const [updateModal, setupdateModal] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [viewModal, setViewModal] = useState(false);
  const [deletModal, setDeletModal] = useState(false);

  const [currentRow, setCurrentRow] = useState();

  const columns = [
    {
      title: "Sr no",
      dataIndex: "srno",
      width: 60,
    },
    {
      title: "Exame Name",
      dataIndex: "examname",
    },
    {
      title: "Total Marks",
      dataIndex: "totalmarks",
      width: 100,
    },
    {
      title: "Date",
      dataIndex: "date",
    },

    {
      title: "Created By",
      dataIndex: "createdby",
    },
    {
      title: "Status",
      dataIndex: "status",
      render: (value, record) => {
        return (
          <>
            <Text
              style={{
                color: record.status === "Live" ? "green" : "",
                fontWeight: record.status === "Live" ? "bolder" : "",
              }}
            >
              {record.status}
            </Text>
          </>
        );
      },
    },

    // {
    //   title: "Action",
    //   dataIndex: "action",

    //   render: (value, record) => {
    //     return (
    //       <>
    //         <EyeTwoTone
    //           style={{ color: "blue" }}
    //           onClick={() => {
    //             setCurrentRow(record.key);
    //             setViewModal(true);
    //           }}
    //         />
    //         <span style={{ padding: 8 }} />
    //         <DeleteFilled
    //           style={{ color: "red" }}
    //           onClick={() => {
    //             setCurrentRow(record.key);
    //             setDeletModal(true);
    //           }}
    //         />
    //         <span style={{ padding: 8 }} />
    //         <EditOutlined
    //           style={{ color: "green" }}
    //           onClick={() => {
    //             setCurrentRow(record.key);
    //             setupdateModal(true);
    //           }}
    //         />
    //       </>
    //     );
    //   },
    // },
    {
      title: "Questions",
      dataIndex: "questions",

      render: (value, record) => {
        return (
          <Button
            style={{
              borderColor: "black",
              background: "white",
              color: "black",
            }}
            className="button"
            type="primary"
            onClick={() => {
              setCurrentRow(record.key);
              navigate(`/admin/exam-questions/${record.key}`);
            }}
          >
            Open
          </Button>
        );
      },
    },
    {
      title: "Show",
      dataIndex: "show",
      render: (value, record) => {
        return (
          <Button
            style={{
              borderColor: "green",
              background: "green",
              color: "white",
            }}
            className="button"
            type="primary"
            onClick={() => navigate(`/admin/student-exam/${record.key}`)}
          >
            Show
          </Button>
        );
      },
    },
  ];
  const data = [];
  for (let i = 0; i < examList.length; i++) {
    data.push({
      key: examList[i].examId,
      srno: i + 1,
      examname: examList[i].examName,
      totalmarks: examList[i].totalMarks,
      date: dayjs(examList[i].date).format("YYYY-MM-DD"),
      createdby: examList[i].userName,
      status:
        dayjs(examList[i].date).format("YYYY-MM-DD").toString() ===
        dayjs().format("YYYY-MM-DD").toString()
          ? "Live"
          : dayjs(examList[i].date).isAfter(dayjs())
          ? "Upcoming"
          : "Completed",
      noOfLogicalQuestion: examList[i].noOfLogicalQuestion,
      noOfTechnicalQuestion: examList[i].noOfTechnicalQuestion,
      noOfProgrammingQuestion: examList[i].noOfProgrammingQuestion,
    });
  }

  console.log("data: ", data);
  // const handleDeleteExam = () => {
  //   axios
  //     .delete(`${"http://localhost:8899"}/question/${currentRow}`)
  //     .then((response) => {
  //       if (response.status === 200) {
  //         setQuestionList((prevQuestionList) => {
  //           return prevQuestionList.filter(
  //             (question) => question.questionId !== currentRow
  //           );
  //         });
  //         message.success("Question deleted successfully");
  //       }
  //     })
  //     .catch((error) => {
  //       message.error(error.response.data.message);
  //     });
  // };

  return (
    <>
      <Table
        pagination={false}
        columns={columns}
        size="middle"
        dataSource={data}
      />

      <UpdateExam
        updateModal={updateModal}
        setUpdateModal={setupdateModal}
        setExamList={setExamList}
        examDetail={examList.find((exam) => exam.ExamId === currentRow)}
      />
      {/* {function ComponentA(props) {
        const navigate = useNavigate();
        setExamList={setExamList};
        examDetail={examList.find((exam) => exam.ExamId === currentRow)}
        const toComponentB = () => {
          navigate("/componentB", { state: { id: 1, name: "sabaoon" } });
        };
      }} */}

      <Modal
        title="Delete Exam"
        style={{
          top: 20,
          width: 700,
        }}
        centered
        open={deletModal}
        onOk={() => setDeletModal(false)}
        onCancel={() => setDeletModal(false)}
      >
        <DeleteModall value="Exam" />
      </Modal>

      <Modal
        title="Exam Details"
        style={{
          top: 20,
          width: 700,
        }}
        centered
        open={viewModal}
        onOk={() => setViewModal(false)}
        onCancel={() => setViewModal(false)}
        cancelButtonProps={{ style: { display: "none" } }}
        okButtonProps={{ style: { display: "none" } }}
      >
        <ExamDetail
          examDetail={examList.find((exam) => exam.examId === currentRow)}
        />
      </Modal>
    </>
  );
};
export default ExamTable;
