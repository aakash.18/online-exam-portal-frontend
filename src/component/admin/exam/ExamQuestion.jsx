import { Row, Space, Col, Divider, Table } from "antd";
import React, { useEffect, useState } from "react";
import SearchExam from "./SearchExam";
import axios from "axios";
import { useOutletContext, useLocation, useParams } from "react-router-dom";

const ExamQuestion = ({ isStudentResultDetail }) => {
  const { setIsLoading } = useOutletContext();
  const [questionList, setQuestionList] = useState(0);
  const params = useParams();
  const data = [];

  useEffect(() => {
    setIsLoading(true);
    if (isStudentResultDetail) {
      axios
        .post(`${"http://localhost:8899"}/exam/detail`, {
          examId: params.examId,
          userId: localStorage.getItem("userId"),
        })
        .then((response) => {
          console.log(response.data.data);
          setQuestionList(response.data.data);
        })
        .finally(() => {
          setIsLoading(false);
        });
    } else {
      axios
        .get(`${"http://localhost:8899"}/exam/${params.examId}`, {
          headers: {
            "Access-Control-Allow-Origin": "*",
          },
        })
        .then((response) => {
          setQuestionList(response.data.data.questionDtoList);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  }, []);

  return (
    <>
      <div className="container">
        <Space direction="horizontal" align="start">
          <span className="span">Exam Questions </span>
        </Space>
       
      </div>
      <Divider />

      {questionList &&
        questionList.map &&
        questionList.map((question, index) => {
          const category = isStudentResultDetail ? question.responseQuestionDto.typeOfCategory : question.typeOfCategory;
          console.log("question");
          return(
          <div>
            <Row>
              <Col style={{ fontWeight: "bold", fontSize: "16px" }}>
                ( {index + 1} ){" "}
                {isStudentResultDetail
                  ? `${question.responseQuestionDto.question}`
                  : `${question.question}`}
              </Col>

              <Col
                span={24}
                style={{ display: "flex", justifyContent: "start" }}
              ></Col>
            </Row>
            {category !== "Programming" ? (
              <>
                <Row>
                  <Col
                    span={12}
                    style={{ display: "flex", justifyContent: "start" }}
                  >
                    ( A ){" "}
                    {isStudentResultDetail
                      ? `${question.responseQuestionDto.opt1}`
                      : `${question.opt1}`}
                  </Col>
                  <Col
                    span={12}
                    style={{ display: "flex", justifyContent: "start" }}
                  >
                    ( B ){" "}
                    {isStudentResultDetail
                      ? `${question.responseQuestionDto.opt2}`
                      : `${question.opt2}`}
                  </Col>
                </Row>
                <Row>
                  <Col
                    span={12}
                    style={{ display: "flex", justifyContent: "start" }}
                  >
                    ( C ){" "}
                    {isStudentResultDetail
                      ? `${question.responseQuestionDto.opt3}`
                      : `${question.opt3}`}
                  </Col>
                  <Col
                    span={12}
                    style={{ display: "flex", justifyContent: "start" }}
                  >
                    ( D ){" "}
                    {isStudentResultDetail
                      ? `${question.responseQuestionDto.opt4}`
                      : `${question.opt4}`}
                  </Col>
                </Row>
              </>
            ) : (
              ""
            )}
            <Row>
              <Col
                span={8}
                style={{ display: "flex", justifyContent: "start" }}
              >
                Category :{" "}
                {isStudentResultDetail
                  ? `${question.responseQuestionDto.typeOfCategory}`
                  : `${question.typeOfCategory}`}
              </Col>
            </Row>
            <Row>
              <Col
                span={8}
                style={{ display: "flex", justifyContent: "start" }}
              >
                Correct Answer :{" "}
                {isStudentResultDetail
                  ? `${question.responseQuestionDto.correctAns}`
                  : `${question.correctAns}`}
              </Col>
            </Row>

            <Row>
              <Col
                span={8}
                style={{ display: "flex", justifyContent: "start" }}
              >
                {isStudentResultDetail
                  ? `Your answer: ${question.studentSelectedAnswer}`
                  : `Created By : ${question.admin}`}
              </Col>
            </Row>
            {isStudentResultDetail && (
              <Row>
                <Col
                  span={8}
                  style={{
                    display: "flex",
                    justifyContent: "start",
                    color: question.obtainedMark === 0 ? "red" : "green",
                  }}
                >
                  Obtained Mark: {question.obtainedMark}
                </Col>
              </Row>
            )}
            <Divider />
          </div>);
        })}
    </>
  );
};

export default ExamQuestion;
