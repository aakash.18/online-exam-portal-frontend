import {
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Modal,
  Row,
  Select,
  TimePicker,
  message,
  Divider,
  FloatButton,
} from "antd";
import axios from "axios";
import dayjs from "dayjs";
import { default as React, useEffect } from "react";
import "./CreateExam.css";
import "./UpdateExam.css";
import FormItem from "antd/es/form/FormItem";

const { TextArea } = Input;
const UpdateExam = ({
  updateModal,
  setUpdateModal,
  examDetail,
  setExamList,
}) => {
  const format = "HH:mm";
  const [formRef] = Form.useForm();
  useEffect(() => {
    if (!examDetail) return;
    formRef.setFieldsValue(examDetail);
  });

  const handleSubmit = () => {
    const values = formRef.getFieldsValue();
    const updateExam = {
      userName: values.userName,
      examName: values.examName,
      examDuration: values.examDuration,
      noOfLogicalQuestion: values.noOfLogicalQuestion,
      noOfTechnicalQuestion: values.noOfTechnicalQuestion,
      noOfProgrammingQuestion: values.noOfProgrammingQuestion,
      date: values.date,
      totalMarks: values.totalMarks,
    };
    axios
      .put(`${"http://localhost:8899"}/exam/${examDetail.examId}`, updateExam)
      .then((response) => {
        message.success("Exam updated successfully");
        formRef.resetFields([]);
        setExamList((prevExamList) => {
          return prevExamList.map((exam) => {
            return exam.examId === examDetail.examId ? updateExam : exam;
          });
        });
        setUpdateModal(false);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });

    return (
      <>
        <Modal
          title="Update Exam"
          width={700}
          style={{
            top: 20,
          }}
          centered
          open={updateModal}
          onOk={handleSubmit}
          onCancel={() => {
            formRef.resetFields([]);
            setUpdateModal(false);
          }}
          okText="Update"
          cancelButtonProps={{ style: { display: "none" } }}
        >
          <Form
            form={formRef}
            labelAlign="left"
            colon={false}
            labelCol={{
              span: 4,
            }}
            wrapperCol={{
              span: 19,
            }}
            layout="horizontal"
            style={{
              width: 470,
            }}
          >
            <div />
            <Row gutter={24}>
              <Form.Item name="examname" label="Exam Name">
                <TextArea rows={2} />
              </Form.Item>
              <Form.Item name="date" label="Date">
                <Col className="component-align" span={6}>
                  <DatePicker style={{ width: 200 }} />
                </Col>
              </Form.Item>

              <Col className="component-align" span={6}>
                <TimePicker
                  defaultValue={dayjs("2:00", format)}
                  format={format}
                  style={{ width: 200 }}
                />
              </Col>
            </Row>
            <div className="gap" />

            <Row>
              <Col className="component-align" span={6}>
                Valid Exam period
              </Col>
              <Col className="component-align" span={6}>
                Logical Passing Marks
              </Col>
              <Col className="component-align" span={6}>
                Technnical Passing Marks
              </Col>
              <Col className="component-align" span={6}>
                Programming Passing Marks
              </Col>
            </Row>
            <div className="gap" />
            <Row>
              <Col className="component-align" span={6}>
                <TimePicker.RangePicker style={{ width: 200 }} />
              </Col>
              <Col className="component-align" span={6}>
                <InputNumber style={{ width: 200 }} />
              </Col>
              <Col className="component-align" span={6}>
                <InputNumber style={{ width: 200 }} />
              </Col>
              <Col className="component-align" span={6}>
                <InputNumber style={{ width: 200 }} />
              </Col>
            </Row>

            <div className="gap" />
            <Divider />
            <Row className="row-element-align">
              <h4>Logical Section</h4>
              <Row>
                <span>Logical Question : </span>
                <span style={{ color: "green", paddingRight: "10px" }}>
                  10{" "}
                </span>
                {/* <span>Logical Marks : </span>
          <span style={{ color: "green" }}>10</span> */}
              </Row>
            </Row>
            <div className="gap" />

            <Row>
              <Col className="component-align" span={6}>
                Number of Questions
              </Col>
              <Col className="component-align" span={6}>
                Difficulty
              </Col>
            </Row>
            <div className="small-gap" />
            <Row>
              <Col className="component-align" span={6}>
                <InputNumber style={{ width: 200 }} />
              </Col>
              <Select
                showSearch
                style={{ width: 225 }}
                placeholder="Search or Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? "").includes(input)
                }
                filterSort={(optionA, optionB) =>
                  (optionA?.label ?? "")
                    .toLowerCase()
                    .localeCompare((optionB?.label ?? "").toLowerCase())
                }
                options={[
                  {
                    value: "EASY",
                    label: "EASY",
                  },
                  {
                    value: "MODERATE",
                    label: "MODERATE",
                  },
                  {
                    value: "ADVANCE",
                    label: "ADVANCE",
                  },
                ]}
              />
              <Col span={2}></Col>
            </Row>
            <div className="small-gap" />
            <Row>
              <Col className="component-align" span={6}>
                <span className="red-text">(4 questions available)</span>
              </Col>
            </Row>
            <div className="gap" />
            <Row className="row-element-align">
              <Divider />
              <h4>Technical Section</h4>
              <Row>
                <span>Technical Question : </span>
                <span style={{ color: "green", paddingRight: "10px" }}>
                  10{" "}
                </span>
              </Row>
            </Row>
            <Row>
              <Col className="component-align" span={6}>
                Number of Questions
              </Col>
              <Col className="component-align" span={6}>
                Difficulty
              </Col>
            </Row>
            <div className="small-gap" />
            <Row>
              <Col className="component-align" span={6}>
                <InputNumber style={{ width: 200 }} />
              </Col>
              <Row className="row-element-align"></Row>
              <Select
                showSearch
                style={{ width: 225 }}
                placeholder="Search or Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? "").includes(input)
                }
                filterSort={(optionA, optionB) =>
                  (optionA?.label ?? "")
                    .toLowerCase()
                    .localeCompare((optionB?.label ?? "").toLowerCase())
                }
                options={[
                  {
                    value: "EASY",
                    label: "EASY",
                  },
                  {
                    value: "MODERATE",
                    label: "MODERATE",
                  },
                  {
                    value: "ADVANCE",
                    label: "ADVANCE",
                  },
                ]}
              />
              <Col span={2}></Col>
            </Row>
            <Row>
              <Col className="component-align" span={6}>
                <span className="red-text">(4 questions available)</span>
              </Col>
            </Row>

            <div className="gap" />
            <Divider />
            <Row className="row-element-align">
              <h4>Programming Section</h4>
              <Row>
                <span>Programming Question : </span>
                <span style={{ color: "green" }}>10</span>
              </Row>
            </Row>
            <Row>
              <Col className="component-align" span={6}>
                Number of Questions
              </Col>
              <Col className="component-align" span={6}>
                Difficulty
              </Col>
            </Row>
            <div className="gap" />
            <Row>
              <Col className="component-align" span={6}>
                <InputNumber style={{ width: 200 }} />
              </Col>
              <Select
                showSearch
                style={{ width: 225 }}
                placeholder="Search or Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option?.label ?? "").includes(input)
                }
                filterSort={(optionA, optionB) =>
                  (optionA?.label ?? "")
                    .toLowerCase()
                    .localeCompare((optionB?.label ?? "").toLowerCase())
                }
                options={[
                  {
                    value: "EASY",
                    label: "EASY",
                  },
                  {
                    value: "MODERATE",
                    label: "MODERATE",
                  },
                  {
                    value: "ADVANCE",
                    label: "ADVANCE",
                  },
                ]}
              />

              <Col span={2}></Col>
            </Row>
            <Row>
              <Col className="component-align" span={6}>
                <span className="red-text">(4 questions available)</span>
              </Col>
            </Row>
            <FloatButton
              shape="square"
              type="primary"
              style={{
                right: 24,
                width: 200,
              }}
              description="Total Question 10 Total Marks : 20"
            />
            <Row style={{ display: "flex", justifyContent: "end" }}>
              <span>Total Question : </span>
              <span
                style={{
                  color: "red",
                  paddingRight: "10px",
                  fontWeight: "bold",
                }}
              >
                10{" "}
              </span>
              <span>Total Marks : </span>
              <span style={{ color: "green", fontWeight: "bold" }}>10</span>
            </Row>
            <Button style={{ width: 225 }} type="primary" block>
              Add
            </Button>
          </Form>
        </Modal>
      </>
    );
  };
};
export default UpdateExam;
