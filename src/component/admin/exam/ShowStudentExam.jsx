import React, { useEffect, useState } from "react";
import { useNavigate, useOutletContext, useParams } from "react-router-dom";
import ShowStudentExamTable from "./ShowStudentExamTable";
import axios from "axios";
import { message } from "antd";

const ShowStudentExam = () => {
  const navigate = useNavigate();
  const params = useParams();
  const { setIsLoading } = useOutletContext();
  const [pendingResultList, setPendingResultList] = useState();
  useEffect(() => {
    setIsLoading(true);
    axios
      .post(`${"http://localhost:8899"}/result/status`, {
        status: false,
        examId: params.examId,
      })
      .then((response) => {
        console.log(response.data.data);
        setPendingResultList(response.data.data);
      })
      .catch((error) => {
        console.log(error.response.data.message);
        message.error(error.response.data.message);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  console.log("pendingResult", pendingResultList);
  return (
    <>
      {pendingResultList && (
        <ShowStudentExamTable
          pendingResultList={pendingResultList}
          examId={params.examId}
        />
      )}
    </>
  );
};

export default ShowStudentExam;
