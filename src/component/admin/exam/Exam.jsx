import React, { useEffect, useState } from "react";
import { Button, Divider, Flex, Radio, Modal, Space } from "antd";
import {
  Link,
  Navigate,
  Outlet,
  useNavigate,
  useOutletContext,
} from "react-router-dom";
import ExamTable from "./ExamTable";
import SearchExam from "./SearchExam";
import FilterModal from "../FilterModal";
import axios from "axios";

const Exam = () => {
  const navigate = useNavigate();
  const { setIsLoading } = useOutletContext();
  const [examList, setExamList] = useState();
  const [categoryPassingMarksList, setCategoryPassinMarksList] = useState();

  useEffect(() => {
    setIsLoading(true);
    axios.get(`${"http://localhost:8899"}/exam/all/`).then((response) => {
      setExamList(response.data.data);
    }).finally(() => {
      setIsLoading(false);
    });
  }, []);

  return (
    <>
      <div className="container">
        <Space direction="horizontal" align="start">
          <span className="span">Exam</span>
        </Space>
        <Space direction="horizontal" align="end">
          
          <Button
            className="button"
            type="primary"
            onClick={() => navigate("/admin/createexam")}
          >
            Add Exam
          </Button>
        </Space>
      </div>
      {examList && (
        <ExamTable
          examList={examList}
          setExamList={setExamList}
          categoryPassingMarksList={categoryPassingMarksList}
          setCategoryPassinMarksList={setCategoryPassinMarksList}
        />
      )}
    </>
  );
};

export default Exam;
