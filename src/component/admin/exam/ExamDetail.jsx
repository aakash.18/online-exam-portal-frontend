import React from "react";
import { Col, Divider, Row, Space } from "antd";
import "./ExamDetail.css";

const ExamDetail = ({ examDetail }) => {
  console.log(examDetail);
  return (
    <div>
      <Row className="question-span">
        <Col span={24}>{examDetail.examName}</Col>
      </Row>
      <Row className="row-span">
        <Col span={8} className="title">
          Logical{" "}
        </Col>
        <Col span={8} className="title">
          Technical{" "}
        </Col>
        <Col span={8} className="title">
          Programming{" "}
        </Col>
      </Row>
      <Row className="row-span">
        <Col span={8}>
          Questions:{" "}
          <span className="text-color">{examDetail.noOfLogicalQuestion}</span>
        </Col>
        <Col span={8}>
          Questions:{" "}
          <span className="text-color">{examDetail.noOfTechnicalQuestion}</span>
        </Col>
        <Col span={8}>
          Questions:{" "}
          <span className="text-color">
            {examDetail.noOfProgrammingQuestion}
          </span>
        </Col>
      </Row>
      <Row className="row-span">
        <Col span={8}>
          Marks:{" "}
          <span className="textF-color">{examDetail.noOfLogicalQuestion}</span>
        </Col>
        <Col span={8}>
          Marks:{" "}
          <span className="text-color">{examDetail.noOfTechnicalQuestion}</span>
        </Col>
        <Col span={8}>
          Marks:{" "}
          <span className="text-color">
            {5 * examDetail.noOfProgrammingQuestion}
          </span>
        </Col>
      </Row>
      <Row className="row-span">
        <Col span={8}>
          Passing Marks: <span className="text-danger">{examDetail.logicalPassingMarks}</span>
        </Col>
        <Col span={8}>
          Pssing Marks: <span className="text-danger">{examDetail.technicalPassingMarks}</span>
        </Col>
        <Col span={8}>
          Passing Marks: <span className="text-danger">{examDetail.programmingPassingMarks}</span>
        </Col>
      </Row>

      <Divider />

      <Row>
        <Col span={12}>
          Total Question:{" "}
          {examDetail.noOfTechnicalQuestion +
            examDetail.noOfLogicalQuestion +
            examDetail.noOfProgrammingQuestion}
        </Col>
        <Col span={12}>Exam Date: {examDetail.date} </Col>
      </Row>
      <Row>
        <Col span={12}>
          Total Marks :{" "}
          {examDetail.noOfTechnicalQuestion +
            examDetail.noOfLogicalQuestion +
            5 * examDetail.noOfProgrammingQuestion}
        </Col>

        <Col span={12}>Created by: {examDetail.userName}</Col>
      </Row>
    </div>
  );
};

export default ExamDetail;
