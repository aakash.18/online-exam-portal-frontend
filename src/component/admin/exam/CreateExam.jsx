import {
  Button,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  InputNumber,
  Row,
  TimePicker,
  message,
} from "antd";
import axios from "axios";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useNavigate, useOutletContext } from "react-router-dom";
import "./CreateExam.css";

const { TextArea } = Input;
const CreateExam = () => {
  const format = "HH:mm";
  const navigate = useNavigate();
  const [formRef] = Form.useForm();

  const [availableLogicalQuestions, setAvailableLogicalQuestions] = useState();
  const [availableTechnicalQuestions, setAvailableTechnicalQuestions] =
    useState();
  const [availableProgrammingQuestions, setAvailableProgrammingQuestions] =
    useState();

  const [totalLogicalQuestion, setTotalLogicalQuestion] = useState(0);
  const [totalTechnicalQuestion, setTotalTechnicalQuestion] = useState(0);
  const [totalProgrammingQuestion, setTotalProgrammingQuestion] = useState(0);

  const { setIsLoading } = useOutletContext();

  const handleSubmit = (values) => {
    console.log(dayjs(values.date).format("DD/MM/YYYY"));
    console.log(dayjs(values.examDuration).hour());
    console.log(dayjs(values.examDuration).minute());
    console.log(dayjs(values.examDuration).second());
    console.log(dayjs(values.validExamPeriod[0]).format("hh:mm:ss A"));
    console.log(dayjs(values.validExamPeriod[1]).format("hh:mm:ss A"));

    console.log(values);
    axios
      .post(`${"http://localhost:8899"}/exam/`, {
        examName: values.examName,
        date: dayjs(values.date).format("YYYY-MM-DD"),
        startTime:
          (dayjs(values.validExamPeriod[0]).hour() * 3600 +
            dayjs(values.validExamPeriod[0]).minute() * 60 +
            dayjs(values.validExamPeriod[0]).second()) *
          1000,
        endTime:
          (dayjs(values.validExamPeriod[1]).hour() * 3600 +
            dayjs(values.validExamPeriod[1]).minute() * 60 +
            dayjs(values.validExamPeriod[1]).second()) *
          1000,
        duration:
          (dayjs(values.examDuration).hour() * 3600 +
            dayjs(values.examDuration).minute() * 60 +
            dayjs(values.examDuration).second()) *
          1000,
        logicalPassingMarks: values.logicalPassingMarks,
        technicalPassingMarks: values.technicalPassingMarks,
        programmingPassingMarks: values.programmingPassingMarks,
        noOfLogicalQuestion: values.noOfLogicalQuestion,
        noOfTechnicalQuestion: values.noOfTechnicalQuestion,
        noOfProgrammingQuestion: values.noOfProgrammingQuestion,
        logicalDifficulty: values.logicalDifficulty,
        technicalDifficulty: values.technicalDifficulty,
        programmingDifficulty: values.programmingDifficulty,
        adminId: localStorage.getItem("userId"),
        totalMarks:
          values.noOfLogicalQuestion +
          values.noOfTechnicalQuestion +
          5 * values.noOfProgrammingQuestion,
      })
      .then((response) => {
        console.log(response.data);
        message.success("Exam added successfully");
        formRef.resetFields([]);
        navigate("/admin/exam");
      })
      .catch((error) => {
        console.log(error.response.data.message);
        message.error(error.response.data.message);
      });
  };

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`${"http://localhost:8899"}/question/category-question-count`)
      .then((response) => {
        setAvailableLogicalQuestions(response.data.data.logicalQuestionCount);
        setAvailableTechnicalQuestions(
          response.data.data.technicalQuestionCount
        );
        setAvailableProgrammingQuestions(
          response.data.data.programmingQuestionCount
        );
      });
    setIsLoading(false);
  }, []);

  return (
    <div>
      <Form
        form={formRef}
        layout="vertical"
        colon={false}
        onFinish={handleSubmit}
      >
        <h4 className="component-align">Create Exam</h4>
        <div className="gap" />
        <Row gutter={24}>
          <Form.Item label="Exam Name" name="examName">
            <Input.TextArea rows="1" cols="62" />
          </Form.Item>
          <Col span={1} />
          <Form.Item label="Date Of Exam " name="date">
            <DatePicker
              format={dayjs().format("YYYY-MM-DD")}
              minDate={dayjs()}
              style={{ width: 200 }}
            />
          </Form.Item>
          <Col span={1} />
          <Form.Item label="Exam Duration" name="examDuration">
            <TimePicker format={format} style={{ width: 200 }} />
          </Form.Item>
        </Row>
        <div className="gap" />

        <Row>
          <Form.Item label="Valid Exam Period " name="validExamPeriod">
            <TimePicker.RangePicker style={{ width: 200 }} />
          </Form.Item>
          <Col span={1} />
          <Form.Item label="Logical Passing marks " name="logicalPassingMarks">
            <InputNumber style={{ width: 200 }} />
          </Form.Item>
          <Col span={1} />
          <Form.Item
            label="Technical Passing Marks "
            name="technicalPassingMarks"
          >
            <InputNumber style={{ width: 200 }} />
          </Form.Item>
          <Col span={1} />
          <Form.Item
            label="Programming passing marks "
            name="programmingPassingMarks"
          >
            <InputNumber style={{ width: 200 }} />
          </Form.Item>
        </Row>
        <Divider />
        <Row className="row-element-align">
          <h4>Logical Section</h4>
          <Row>
            <span>Logical Question : </span>
            <span style={{ color: "green", paddingRight: "10px" }}>
              {totalLogicalQuestion}{" "}
            </span>
          </Row>
        </Row>

        <div className="small-gap" />
        <Row>
          <Form.Item label="Number of Question" name="noOfLogicalQuestion">
            <InputNumber
              max={availableLogicalQuestions}
              onChange={(value) => {
                setTotalLogicalQuestion(value);
              }}
              style={{ width: 200, height: 30 }}
            />
          </Form.Item>
          <Col span={2} />
          <Col span={2} />
        </Row>
        <Row>
          <Col className="component-align" span={6}>
            <span className="red-text">
              ({availableLogicalQuestions} questions available)
            </span>
          </Col>
        </Row>

        <Row className="row-element-align">
          <Divider />
          <h4>Technical Section</h4>
          <Row>
            <span>Technical Question : </span>
            <span style={{ color: "green", paddingRight: "10px" }}>
              {totalTechnicalQuestion}{" "}
            </span>
          </Row>
        </Row>

        <div className="small-gap" />
        <Row>
          <Form.Item label="Number of Question" name="noOfTechnicalQuestion">
            <InputNumber
              max={availableTechnicalQuestions}
              onChange={(value) => {
                setTotalTechnicalQuestion(value);
              }}
              style={{ width: 200, height: 30 }}
            />
          </Form.Item>
          <Col span={2} />
          {/* <Form.Item label="Difficulty" name="technicalDifficulty">
            <Select
              showSearch
              style={{ width: 225 }}
              placeholder="Search or Select"
              optionFilterProp="children"
              filterOption={(input, option) =>
                (option?.label ?? "").includes(input)
              }
              filterSort={(optionA, optionB) =>
                (optionA?.label ?? "")
                  .toLowerCase()
                  .localeCompare((optionB?.label ?? "").toLowerCase())
              }
              options={[
                {
                  value: "EASY",
                  label: "EASY",
                },
                {
                  value: "MODERATE",
                  label: "MODERATE",
                },
                {
                  value: "ADVANCE",
                  label: "ADVANCE",
                },
              ]}
            />
          </Form.Item> */}
          <Col span={2} />
        </Row>
        <Row>
          <Col className="component-align" span={6}>
            <span className="red-text">
              ({availableTechnicalQuestions} questions available)
            </span>
          </Col>
        </Row>

        <Divider />
        <Row className="row-element-align">
          <h4>Programming Section</h4>
          <Row>
            <span>Programming Question : </span>
            <span style={{ color: "green", paddingRight: "10px" }}>
              {totalProgrammingQuestion}{" "}
            </span>
          </Row>
        </Row>

        <div className="small-gap" />
        <Row>
          <Form.Item label="Number of Question" name="noOfProgrammingQuestion">
            <InputNumber
              max={availableProgrammingQuestions}
              onChange={(value) => {
                setTotalProgrammingQuestion(value);
              }}
              style={{ width: 200, height: 30 }}
            />
          </Form.Item>
          <Col span={2} />
          {/* <Form.Item label="Difficulty" name="programmingDifficulty">
            <Select
              showSearch
              style={{ width: 225 }}
              placeholder="Search or Select"
              optionFilterProp="children"
              filterOption={(input, option) =>
                (option?.label ?? "").includes(input)
              }
              filterSort={(optionA, optionB) =>
                (optionA?.label ?? "")
                  .toLowerCase()
                  .localeCompare((optionB?.label ?? "").toLowerCase())
              }
              options={[
                {
                  value: "EASY",
                  label: "EASY",
                },
                {
                  value: "MODERATE",
                  label: "MODERATE",
                },
                {
                  value: "ADVANCE",
                  label: "ADVANCE",
                },
              ]}
            />
          </Form.Item> */}
          <Col span={2} />
        </Row>
        <Row>
          <Col className="component-align" span={6}>
            <span className="red-text">
              ({availableProgrammingQuestions} questions available)
            </span>
          </Col>
        </Row>
        <Row style={{ display: "flex", justifyContent: "end" }}>
          <span>Total Question : </span>
          <span
            style={{ color: "red", paddingRight: "10px", fontWeight: "bold" }}
          >
            {Number(totalLogicalQuestion) +
              Number(totalTechnicalQuestion) +
              Number(totalProgrammingQuestion)}
          </span>
          <span>Total Marks :</span>
          <span style={{ color: "green", fontWeight: "bold" }}>
            {" "}
            {Number(totalLogicalQuestion) +
              Number(totalTechnicalQuestion) +
              5 * Number(totalProgrammingQuestion)}
          </span>
        </Row>
        <Button htmlType="submit" style={{ width: 225 }} type="primary" block>
          Add
        </Button>
      </Form>
    </div>
  );
};

export default CreateExam;
