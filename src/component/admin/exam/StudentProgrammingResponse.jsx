import {
  Button,
  Col,
  Form,
  Input,
  InputNumber,
  Row,
  Space,
  message,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useOutletContext, useParams } from "react-router-dom";

const StudentProgrammingResponse = () => {
  const navigate = useNavigate();
  const params = useParams();
  const [formRef] = Form.useForm();
  const [answerList, setAnswerList] = useState();
  const { setIsLoading } = useOutletContext();
  const [userName, setUserName] = useState();
  useEffect(() => {
    setIsLoading(true);
    axios
      .post(`${"http://localhost:8899"}/selected-answer/category`, {
        examId: params.examId,
        userId: params.userId,
        categoryId: 5,
      })
      .then((response) => {
        const responseAnswerList = response.data.data;
        const data = [];
        for (let index = 0; index < responseAnswerList.length; index++) {
          if (index === 0) {
            setUserName(responseAnswerList[index].userDto.name);
          }
          data.push({
            studentSelectedAnswerId:
              responseAnswerList[index].studentSelectedAnswerId,
            selectedAnswer: responseAnswerList[index].selectedAnswer,
            question: responseAnswerList[index].questionDto.question,
            questionId: responseAnswerList[index].questionDto.questionId,
          });
        }
        setAnswerList(data);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  const onSubmit = () => {
    let totalProgramminMarks = 0;
    const selectedAnswerToMarkList = [];
    answerList.forEach((answer) => {
      totalProgramminMarks += answer.mark;
      selectedAnswerToMarkList.push({
        selectedAnswerId: answer.studentSelectedAnswerId,
        mark: answer.mark,
      });
    });
    axios
      .post(`${"http://localhost:8899"}/result/generate-result`, {
        examId: params.examId,
        userId: params.userId,
        programmingMark: totalProgramminMarks,
        selectedAnswerToMark: selectedAnswerToMarkList,
      })
      .then((response) => {
        message.success(response.data.data);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  return (
    <div className="container">
      {answerList && (
        <Form form={formRef} layout="vertical" colon={false}>
          <h6
            style={{
              height: "20",
              marginBottom: "30",
              display: "flex",
              justifyContent: "start",
            }}
          >
            Student Name : {userName}
          </h6>
          {answerList.map((_, index) => {
            return (
              <div>
                <Row>
                  <Col
                    span={24}
                    style={{ display: "flex", justifyContent: "flex-start" }}
                  >
                    {answerList[index].question}
                  </Col>
                </Row>
                <Space direction="vertical" align="end">
                  <Space align="end">
                    <InputNumber
                      placeholder="Marks"
                      max={5}
                      onChange={(value) => {
                        setAnswerList((prevList) =>
                          prevList.map((e) => {
                            if (e.questionId === answerList[index].questionId) {
                              return {
                                ...e,
                                mark: value,
                              };
                            } else return e;
                          })
                        );
                      }}
                    />
                  </Space>
                  <Input.TextArea
                    value={answerList[index].selectedAnswer}
                    readOnly={true}
                    rows="10"
                    cols="150"
                  />
                </Space>
              </div>
            );
          })}
          <Space direction="horizontal" style={{ paddingTop: "16px" }}>
            <Button onClick={onSubmit} type="primary">
              Submit
            </Button>
          </Space>
        </Form>
      )}
    </div>
  );
};

export default StudentProgrammingResponse;
