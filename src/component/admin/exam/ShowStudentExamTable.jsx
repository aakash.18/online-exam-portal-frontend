import { Button, Table, message } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useOutletContext, useParams } from "react-router-dom";

const ShowStudentExamTable = ({pendingResultList, examId}) => {
  const navigate = useNavigate();
  const data = [];

  const columns = [
    {
      title: "Sr no",
      dataIndex: "srno",
      width: 60,
    },
    {
      title: "Student Name",
      dataIndex: "studentName",
    },
    {
      title: "Logical Status",
      dataIndex: "logicalStatus",
      width: 130,
    },
    {
      title: "Logical Marks",
      dataIndex: "logicalMarks",
      width: 130,
    },
    {
      title: "Technical Status",
      dataIndex: "technicalStatus",
      width: 130,
    },
    {
      title: "Technical Marks",
      dataIndex: "technicalMarks",
      width: 130,
    },
    {
      title: "Action",
      dataIndex: "action",
      render: (value, record) => {
        return (
          <Button
            type="primary"
            onClick={() => navigate(`/admin/student-exam/${examId}/${record.userId}`)}
          >
            Open
          </Button>
        );
      },
    },
  ];
  for (let i = 0; i < pendingResultList.length; i++) {
    data.push({
      key: pendingResultList[i].responseResultDto.resultId,
      srno: i + 1,
      studentName: pendingResultList[i].responseResultDto.userName,
      logicalMarks: pendingResultList[i].responseResultDto.logicalMarks,
      logicalStatus: pendingResultList[i].responseResultDto.logicalStatus ? "PASS" : "FAIL",
      technicalMarks: pendingResultList[i].responseResultDto.technicalMarks,
      technicalStatus: pendingResultList[i].responseResultDto.technicalStatus ? "PASS" : "FAIL" ,
      userId: pendingResultList[i].responseResultDto.userId
    });
  }
  console.log(data)

  return (
    <>
      {pendingResultList && (
        <Table
          pagination={false}
          columns={columns}
          size="middle"
          dataSource={data}
        />
      )}
    </>
  );
};

export default ShowStudentExamTable;
