// import React from "react";
// import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
// import { Button, Form, Input, InputNumber, Space } from "antd";
// import "./DynamicInput.css";
// const onFinish = (values) => {
//   console.log("Received values of form:", values);
// };
// const DynamicInput = () => (
//   <Form
//     name="dynamic_form_nest_item"
//     onFinish={onFinish}
//     style={{
//       maxWidth: 600,
//     }}
//     autoComplete="off"
//   >
//     <Form.List name="users">
//       {(fields, { add, remove }) => (
//         <>
//           {fields.map(({ key, name, ...restField }) => (
//             <Space
//               key={key}
//               style={{
//                 display: "flex",
//                 marginBottom: 8,
//               }}
//               align="baseline"
//             >
//               <Form.Item
//                 {...restField}
//                 name={[name, "first"]}
//                 rules={[
//                   {
//                     required: true,
//                     message: "Missing Mark Per Question",
//                   },
//                 ]}
//               >
//                 <InputNumber
//                   style={{
//                     width: 200,
//                   }}
//                   placeholder="Mark Per Question"
//                 />
//               </Form.Item>

//               <Form.Item
//                 {...restField}
//                 name={[name, "last"]}
//                 rules={[
//                   {
//                     required: true,
//                     message: "Missing No of Question ",
//                   },
//                 ]}
//               >
//                 <InputNumber
//                   style={{
//                     width: 200,
//                   }}
//                   placeholder="No of Question "
//                 />
//               </Form.Item>

//               <Form.Item>
//                 <span className="red-text">(4 questions available)</span>
//               </Form.Item>

//               <MinusCircleOutlined onClick={() => remove(name)} />
//             </Space>
//           ))}
//           <span></span>
//           <Form.Item>
//             <Button
//               className="button"
//               type="dashed"
//               onClick={() => add()}
//               block
//               icon={<PlusOutlined />}
//             >
//               Add field
//             </Button>
//           </Form.Item>
//         </>
//       )}
//     </Form.List>
//   </Form>
// );
// export default DynamicInput;
// //DynamicInput
// //className="button"
