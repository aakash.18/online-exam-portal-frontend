import { Button, Form, Input, Space, message } from "antd";
import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";

const { TextArea } = Input;
const CreateStudent = () => {
  const navigate = useNavigate();
  const [formRef] = Form.useForm();
  const handleSubmit = (values) => {
    axios
      .post(`${"http://localhost:8899"}/user/`, {
        userName: values.userName,
        password: values.password,
        name: values.name,
        userRole: "STUDENT",
      })
      .then((response) => {
        console.log(response.data);
        message.success("Student added successfully");
        formRef.resetFields([]);
        navigate("/admin/student");
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  return (
    <div className="container">
      <Form
        form={formRef}
        layout="vertical"
        colon={false}
        onFinish={handleSubmit}
      >
        <Space direction="vertical" align="start">
          <h4>Student Information</h4>

          <Space direction="horizontal" align="start">
            <Form.Item label="Name" name="name">
              <Input.TextArea rows="1" cols="150" />
            </Form.Item>
            <Form.Item label="UserName" name="userName">
              <Input.TextArea rows="1" cols="150" />
            </Form.Item>
          </Space>
          <Space direction="horizontal" align="start">
            <Form.Item label="Password" name="password">
              <Input.TextArea rows="1" cols="63" />
            </Form.Item>
          </Space>

          <div style={{ height: 20 }} />
          <Space align="end">
            <Button
              htmlType="submit"
              background-color="black"
              style={{ width: 225 }}
              type="primary"
              block
            >
              Save
            </Button>
          </Space>
        </Space>
      </Form>
    </div>
  );
};

export default CreateStudent;
