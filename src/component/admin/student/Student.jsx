import React, { useEffect, useState } from "react";
import { Button, Space } from "antd";
import axios from "axios";
import { useNavigate, useOutletContext } from "react-router-dom";
import SearchBar from "../SearchBar";
import StudentTable from "./StudentTable";

const Student = () => {
  const navigate = useNavigate();
  const { setIsLoading } = useOutletContext();
  const [studentList, setStudentList] = useState();
  useEffect(() => {
    setIsLoading(true);
    axios.get(`${"http://localhost:8899"}/user/all/students`).then((response) => {
      setStudentList(response.data.data);
    });
    setIsLoading(false);
  }, []);

  return (
    <>
      <div className="container">
        <span className="span">Student</span>
        <Space direction="horizontal" align="end">
          <Button className="button" type="primary" onClick={() => navigate("/admin/createstudent")}>
            Add Student
          </Button>
        </Space>
      </div>

      { studentList &&  <StudentTable studentList={studentList} setStudentList={setStudentList} />}
    </>
  );
};

export default Student;


