import React from "react";
import "./ViewStudent.css";

const ViewStudent = ({ ViewStudent }) => {
  return (
    <div>
      <span>Name: {ViewStudent.name}</span>
      <br/>
      <span>Username: {ViewStudent.userName}</span>
    </div>
  );
};

export default ViewStudent;
