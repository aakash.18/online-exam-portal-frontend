import { SearchOutlined } from "@ant-design/icons";
import { Select } from "antd";
import React from "react";
const onChange = (value) => {
  console.log(`selected ${value}`);
  
};
const onSearch = (value) => {
  console.log("search:", value);
};

const filterOption = (input, option) =>
  (option?.label ?? "").toLowerCase().includes(input.toLowerCase());

const SearchStudent = (props) => (
  <Select
  
    style={{ width:200}}
    suffixIcon={<SearchOutlined />}
    showSearch
    
    placeholder="Search Question"
    optionFilterProp="children"
    onChange={onChange}
    onSearch={onSearch}
    filterOption={filterOption}
    
    options={[
      {
        value: "q1",
        label: "Question 1",
      },
      {
        value: "lucy",
        label: "Lucy",
      },
      {
        value: "tom",
        label: "Tom",
      },
    ]}
  />
);
export default SearchStudent;
