import {
  DeleteFilled,
  EditOutlined,
  EyeTwoTone,
  LockFilled,
} from "@ant-design/icons";
import { Modal, Table, Tooltip, message } from "antd";
import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import DeleteModal from "../DeleteModall";
import UpdateStudent from "./UpdateStudent";
import ViewStudent from "./ViewStudent";

const StudentTable = ({ studentList, setStudentList }) => {
  const navigate = useNavigate();
  const [updateModal, setupdateModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [viewModal, setViewModal] = useState(false);

  const [currentRow, setCurrentRow] = useState();

  const columns = [
    {
      title: "Sr no",
      dataIndex: "srno",
      width: 60,
    },
    {
      title: "Student Name",
      dataIndex: "studentname",
      ellipsis: {
        showTitle: false,
      },
      render: (studentname) => (
        <Tooltip placement="topLeft" title={studentname}>
          {studentname}
        </Tooltip>
      ),
    },
    {
      title: "User Name",
      dataIndex: "username",
    },
    {
      title: "Password",
      dataIndex: "password",

      render: (value, record) => {
        return <LockFilled />;
      },
    },
    {
      title: "Action",

      dataIndex: "detail",
      render: (value, record) => {
        return (
          <>
            <EyeTwoTone
              style={{ color: "blue" }}
              onClick={() => {
                setCurrentRow(record.key);
                setViewModal(true);
              }}
            />
            <span style={{ padding: 15 }} />
            <DeleteFilled
              style={{ color: "red" }}
              onClick={() => {
                setCurrentRow(record.key);
                setDeleteModal(true);
              }}
            />
            <span style={{ padding: 15 }} />
            <EditOutlined
              style={{ color: "green" }}
              onClick={() => {
                setCurrentRow(record.key);
                setupdateModal(true);
              }}
            />
          </>
        );
      },
    },
  ];
  const data = [];
  for (let i = 0; i < studentList.length; i++) {
    data.push({
      key: studentList[i].userId,
      srno: i + 1,
      studentname: studentList[i].name,
      username: studentList[i].userName,
      password: "********",
      // createdby: "admin",
    });
  }

  const handleDeleteQuestion = () => {
    axios
      .delete(`${"http://localhost:8899"}/user/${currentRow}`)
      .then((response) => {
        if (response.status === 200) {
          setStudentList((prevQuestionList) => {
            return prevQuestionList.filter(
              (question) => question.questionId !== currentRow
            );
          });
          message.success("student deleted successfully");
        }
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  return (
    <>
      <Table
        pagination={false}
        columns={columns}
        size="middle"
        dataSource={data}
      />
      
      
      <UpdateStudent 
         updateModal={updateModal}
         setUpdateModal={setupdateModal}
         setStudentList={setStudentList}
         ViewStudent={studentList.find(
           (student) => student.userId === currentRow
         )} />

      <Modal
        title="Student Detail "
        style={{
          width: 700,
        }}
        centered
        open={viewModal}
        onOk={() => setViewModal(false)}
        onCancel={() => setViewModal(false)}
        cancelButtonProps={{ style: { display: "none" } }}
        okButtonProps={{ style: { display: "none" } }}
      >
        <ViewStudent
          // ViewStudent={studentList.find(
          //   (student) => student.userId === currentRow
          // )}

          ViewStudent={studentList.find(
            (student) => student.userId === currentRow
          )}
        />
      </Modal>

      <Modal
        title="Delete "
        centered
        style={{
          top: 20,
          width: 700,
        }}
        open={deleteModal}
        onOk={() => {
          handleDeleteQuestion();
          setDeleteModal(false);
        }}
        onCancel={() => setDeleteModal(false)}
        okText="Delete"
      >
        <DeleteModal value="Student" />
      </Modal>
    </>
  );
};
export default StudentTable;
