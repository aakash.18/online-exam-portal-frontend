import { Form, Input, Modal, message } from "antd";
import axios from "axios";
import React, { useEffect } from "react";

const { TextArea } = Input;
const normFile = (e) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};
const UpdateStudent = ({
  updateModal,
  setUpdateModal,
  ViewStudent,
  setStudentList,
}) => {
  const [formRef] = Form.useForm();

  useEffect(() => {
    if (!ViewStudent) return;
    formRef.setFieldsValue(ViewStudent);
  });

  const handleSubmit = () => {
    const values = formRef.getFieldsValue();
    const updateStudent = {
      name: values.name,
      userName: values.userName,
    };
    axios
      .put(
        `${"http://localhost:8899"}/user/${ViewStudent.userId}`,
        updateStudent
      )
      .then((response) => {
        message.success("Student updated successfully");
        formRef.resetFields([]);
        setStudentList((prevStudentList) => {
          return prevStudentList.map((student) => {
            return student.userId === ViewStudent.userId
              ? updateStudent
              : student;
          });
        });
        setUpdateModal(false);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  return (
    <>
      <Modal
        title="Edit "
        centered
        style={{
          top: 20,
          width: 700,
        }}
        
        open={updateModal}
        onOk={handleSubmit}
        setUpdateModal={setUpdateModal}
        setStudentList={setStudentList}
        onCancel={() => {
          formRef.resetFields([]);
          setUpdateModal(false);
        }}
      >
        <Form
        form={formRef}
          labelAlign="left"
          colon={false}
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 19,
          }}
          layout="horizontal"
          style={{
            width: 470,
          }}
        >
          <Form.Item label="Name" name="name">
            <TextArea rows={2} />
          </Form.Item>

          <Form.Item label="Username" name="userName">
            <TextArea rows={2} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
export default UpdateStudent;
