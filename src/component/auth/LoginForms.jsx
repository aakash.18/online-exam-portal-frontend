import { Button, Form, Input, message } from "antd";
import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";
import "./Login.css";

const LoginForm = (props) => {
  const navigate = useNavigate();

  const handleSubmit = (value) => {
    axios
      .post(`${"http://localhost:8899"}/user/login`, {
        userName: value.username,
        password: value.password,
        userRole: props.student ? "STUDENT" : "ADMIN",
      })
      .then((response) => {
        console.log(response.data);
        message.success("Logged in Successfull");
        if (response.data.data.userRole === "ADMIN") {
          navigate("/admin");
        } else {
          navigate("/student");
        }
        localStorage.setItem("userRole", response.data.data.userRole);
        localStorage.setItem("userId", response.data.data.userId);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 12,
      }}
      style={{
        maxWidth: 600,
        width: 500,
        height: 250,
      }}
      colon={false}
      onFinish={handleSubmit}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>
      <div className="login-submit-btn">
        <Button type="primary" htmlType="submit">
          Login
        </Button>
      </div>

      {props.student && (
        <div className="auth-hint">
          <span>
            Note: Student cannot creat account , contact admin if needed!
          </span>
        </div>
      )}
    </Form>
  );
};

export default LoginForm;
