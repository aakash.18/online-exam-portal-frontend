import { Image, Tabs } from "antd";
import React from "react";
import LoginForm from "../auth/LoginForms";
import "./Login.css";

const Login = () => {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        backgroundColor: "#d3d3d3",
        zIndex: -1,
      }}
    >
      <div style={{ backgroundColor: "white", height: "fit-content" }}>
        <div className="app-logo">
          <Image
            src="https://sudishworld.com/icon/online.png"
            width={50}
            height={50}
          ></Image>
        </div>
        <Tabs
          centered
          style={{
            paddingLeft: 30,
            paddingRight: 30,
            margin: 3,
          }}
        >
          <Tabs.tab tab="Student" key="student">
            <LoginForm student />
          </Tabs.tab>
          <Tabs.tab tab="Admin" key="admin">
            <LoginForm />
          </Tabs.tab>
        </Tabs>
      </div>
    </div>
  );
};

export default Login;
