import "bootstrap/dist/css/bootstrap.min.css";
import AppRoutes from "./routes/AppRoutes";
import { ConfigProvider } from "antd";

function App() {
  return (
    <>
      <ConfigProvider theme={{ token: {colorPrimary: "#6943ce"} }}>
        <AppRoutes />
      </ConfigProvider>
    </>
  );
}

export default App;
// 0b0d1b